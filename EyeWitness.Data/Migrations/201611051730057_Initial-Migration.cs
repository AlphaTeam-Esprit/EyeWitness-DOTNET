namespace EyeWitness.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class InitialMigration : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(unicode: false),
                        Status = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ChoosenCriterias",
                c => new
                    {
                        CategoryId = c.Int(nullable: false),
                        CriteriasId = c.Int(nullable: false),
                        vote = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CategoryId, t.CriteriasId })
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Criterias", t => t.CriteriasId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.CriteriasId);
            
            CreateTable(
                "dbo.Criterias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.WitnessCards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        status = c.String(unicode: false),
                        name = c.String(unicode: false),
                        description = c.String(unicode: false),
                        picture = c.String(unicode: false),
                        note = c.Single(nullable: false),
                        etat = c.String(unicode: false),
                        creator_Id = c.Int(nullable: false),
                        agent_Id = c.Int(nullable: false),
                        category_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.creator_Id)
                .ForeignKey("dbo.Users", t => t.agent_Id)
                .ForeignKey("dbo.Categories", t => t.category_Id)
                .Index(t => t.creator_Id)
                .Index(t => t.agent_Id)
                .Index(t => t.category_Id);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        firstName = c.String(unicode: false),
                        lastName = c.String(unicode: false),
                        birthDate = c.DateTime(nullable: false, precision: 0),
                        password = c.String(unicode: false),
                        email = c.String(unicode: false),
                        address = c.String(unicode: false),
                        avatar = c.String(unicode: false),
                        phoneNumber = c.String(unicode: false),
                        noteXP = c.Single(nullable: false),
                        role = c.String(unicode: false),
                        status = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Evaluations",
                c => new
                    {
                        WitnessCardId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        evaluationDate = c.DateTime(nullable: false, precision: 0),
                        comment = c.String(unicode: false),
                        video = c.String(unicode: false),
                        rating = c.Int(nullable: false),
                        picture = c.String(unicode: false),
                    })
                .PrimaryKey(t => new { t.WitnessCardId, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.WitnessCards", t => t.WitnessCardId, cascadeDelete: true)
                .Index(t => t.WitnessCardId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.SubscriptionsWCs",
                c => new
                    {
                        WitnessCardId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        subscriptionDate = c.DateTime(nullable: false, precision: 0),
                        Type = c.String(unicode: false),
                    })
                .PrimaryKey(t => new { t.WitnessCardId, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.WitnessCards", t => t.WitnessCardId, cascadeDelete: true)
                .Index(t => t.WitnessCardId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Competitions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        theme = c.String(unicode: false),
                        descripiton = c.String(unicode: false),
                        dateFrom = c.DateTime(nullable: false, precision: 0),
                        dateTo = c.DateTime(nullable: false, precision: 0),
                        WitnessCard_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WitnessCards", t => t.WitnessCard_Id)
                .Index(t => t.WitnessCard_Id);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(unicode: false),
                        description = c.String(unicode: false),
                        dateFrom = c.DateTime(nullable: false, precision: 0),
                        dateTo = c.DateTime(nullable: false, precision: 0),
                        picture = c.String(unicode: false),
                        witnessCard_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WitnessCards", t => t.witnessCard_Id)
                .Index(t => t.witnessCard_Id);
            
            CreateTable(
                "dbo.Followers",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        FollowerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.FollowerId })
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.Users", t => t.FollowerId)
                .Index(t => t.UserId)
                .Index(t => t.FollowerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.Events", "witnessCard_Id", "dbo.WitnessCards");
            DropForeignKey("dbo.Competitions", "WitnessCard_Id", "dbo.WitnessCards");
            DropForeignKey("dbo.WitnessCards", "category_Id", "dbo.Categories");
            DropForeignKey("dbo.WitnessCards", "agent_Id", "dbo.Users");
            DropForeignKey("dbo.WitnessCards", "creator_Id", "dbo.Users");
            DropForeignKey("dbo.SubscriptionsWCs", "WitnessCardId", "dbo.WitnessCards");
            DropForeignKey("dbo.SubscriptionsWCs", "UserId", "dbo.Users");
            DropForeignKey("dbo.Followers", "FollowerId", "dbo.Users");
            DropForeignKey("dbo.Followers", "UserId", "dbo.Users");
            DropForeignKey("dbo.Evaluations", "WitnessCardId", "dbo.WitnessCards");
            DropForeignKey("dbo.Evaluations", "UserId", "dbo.Users");
            DropForeignKey("dbo.ChoosenCriterias", "CriteriasId", "dbo.Criterias");
            DropForeignKey("dbo.ChoosenCriterias", "CategoryId", "dbo.Categories");
            DropIndex("dbo.Followers", new[] { "FollowerId" });
            DropIndex("dbo.Followers", new[] { "UserId" });
            DropIndex("dbo.Events", new[] { "witnessCard_Id" });
            DropIndex("dbo.Competitions", new[] { "WitnessCard_Id" });
            DropIndex("dbo.SubscriptionsWCs", new[] { "UserId" });
            DropIndex("dbo.SubscriptionsWCs", new[] { "WitnessCardId" });
            DropIndex("dbo.Evaluations", new[] { "UserId" });
            DropIndex("dbo.Evaluations", new[] { "WitnessCardId" });
            DropIndex("dbo.WitnessCards", new[] { "category_Id" });
            DropIndex("dbo.WitnessCards", new[] { "agent_Id" });
            DropIndex("dbo.WitnessCards", new[] { "creator_Id" });
            DropIndex("dbo.ChoosenCriterias", new[] { "CriteriasId" });
            DropIndex("dbo.ChoosenCriterias", new[] { "CategoryId" });
            DropTable("dbo.Followers");
            DropTable("dbo.Events");
            DropTable("dbo.Competitions");
            DropTable("dbo.SubscriptionsWCs");
            DropTable("dbo.Evaluations");
            DropTable("dbo.Users");
            DropTable("dbo.WitnessCards");
            DropTable("dbo.Criterias");
            DropTable("dbo.ChoosenCriterias");
            DropTable("dbo.Categories");
        }
    }
}
