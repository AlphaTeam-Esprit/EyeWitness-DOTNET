// <auto-generated />
namespace EyeWitness.Data.Migrations
{
    using System.CodeDom.Compiler;
    using System.Data.Entity.Migrations;
    using System.Data.Entity.Migrations.Infrastructure;
    using System.Resources;
    
    [GeneratedCode("EntityFramework.Migrations", "6.1.3-40302")]
    public sealed partial class renamepicturesandaddingseparatorinevaluation : IMigrationMetadata
    {
        private readonly ResourceManager Resources = new ResourceManager(typeof(renamepicturesandaddingseparatorinevaluation));
        
        string IMigrationMetadata.Id
        {
            get { return "201611091353456_rename-pictures-and-adding-separator-in-evaluation"; }
        }
        
        string IMigrationMetadata.Source
        {
            get { return null; }
        }
        
        string IMigrationMetadata.Target
        {
            get { return Resources.GetString("Target"); }
        }
    }
}
