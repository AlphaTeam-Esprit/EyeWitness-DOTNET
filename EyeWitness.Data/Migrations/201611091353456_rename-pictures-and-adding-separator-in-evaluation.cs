namespace EyeWitness.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class renamepicturesandaddingseparatorinevaluation : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.Evaluations", "pictures", c => c.String(unicode: false));
            DropColumn("dbo.Evaluations", "picture");
        }
        
        public override void Down()
        {
            AddColumn("dbo.Evaluations", "picture", c => c.String(unicode: false));
            DropColumn("dbo.Evaluations", "pictures");
        }
    }
}
