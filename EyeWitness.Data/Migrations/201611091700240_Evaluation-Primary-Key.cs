namespace EyeWitness.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class EvaluationPrimaryKey : DbMigration
    {
        public override void Up()
        {
            DropPrimaryKey("Evaluations");
            AddPrimaryKey("Evaluations", new[] { "WitnessCardId", "UserId", "evaluationDate" });
        }
        
        public override void Down()
        {
            DropPrimaryKey("Evaluations");
            AddPrimaryKey("Evaluations", new[] { "WitnessCardId", "UserId" });
        }
    }
}
