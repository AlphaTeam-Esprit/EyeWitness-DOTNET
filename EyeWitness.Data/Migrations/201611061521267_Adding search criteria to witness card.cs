namespace EyeWitness.Data.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class Addingsearchcriteriatowitnesscard : DbMigration
    {
        public override void Up()
        {
            AddColumn("dbo.WitnessCards", "Ambience", c => c.String(unicode: false));
            AddColumn("dbo.WitnessCards", "OfferedServices", c => c.String(unicode: false));
            AddColumn("dbo.WitnessCards", "MeansOfPayment", c => c.String(unicode: false));
            AddColumn("dbo.WitnessCards", "Longitude", c => c.Single(nullable: false));
            AddColumn("dbo.WitnessCards", "Latitude", c => c.Single(nullable: false));
            AddColumn("dbo.WitnessCards", "Address", c => c.String(unicode: false));
        }
        
        public override void Down()
        {
            DropColumn("dbo.WitnessCards", "Address");
            DropColumn("dbo.WitnessCards", "Latitude");
            DropColumn("dbo.WitnessCards", "Longitude");
            DropColumn("dbo.WitnessCards", "MeansOfPayment");
            DropColumn("dbo.WitnessCards", "OfferedServices");
            DropColumn("dbo.WitnessCards", "Ambience");
        }
    }
}
