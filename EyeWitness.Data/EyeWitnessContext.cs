﻿
using EyeWitness.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Data.Entity;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Data
{
    //[DbConfigurationType(typeof(MySql.Data.Entity.MySqlEFConfiguration))]
    public class EyeWitnessContext : DbContext
    {
        public EyeWitnessContext(): base("name=WitnessConnectionString")/*base("name=eyewitnessDB")*/
        {

        }

        public DbSet<User> User { get; set; }
        public DbSet<WitnessCard> WitnessCard { get; set; }
        public DbSet<SubscriptionsWC> SubscriptionsWC { get; set; }
        public DbSet<Evaluation> Evaluation { get; set; }
        public DbSet<Competitions> Competitions { get; set; }
        public DbSet<Events> Events { get; set; }
        public DbSet<Category> Category { get; set; }
        public DbSet<ChoosenCriterias> ChoosenCriterias { get; set; }
        public DbSet<Criterias> Criterias { get; set; }
        public DbSet<SuggestionWC> SuggestionWCs { get; set; }
        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ChoosenCriterias>().HasKey(cc => new { cc.CategoryId, cc.CriteriasId });
            modelBuilder.Entity<SubscriptionsWC>().HasKey(sub => new { sub.WitnessCardId, sub.UserId });
            modelBuilder.Entity<Evaluation>().HasKey(ev => new { ev.WitnessCardId, ev.UserId, ev.evaluationDate });
            modelBuilder.Entity<User>().HasMany(w => w.witnessCardManaged).WithOptional(a => a.agent).WillCascadeOnDelete(false);
            modelBuilder.Entity<User>().HasMany(w => w.witnessCardCreated).WithRequired(a => a.creator).WillCascadeOnDelete(false);
            modelBuilder.Entity<User>().HasMany(f => f.followers).WithMany().Map(m =>
            {
                m.MapLeftKey("UserId");
                m.MapRightKey("FollowerId");
                m.ToTable("Followers");
            }
            );
            modelBuilder.Entity<SuggestionWC>().HasKey(c => new { c.SuggestingUserId, c.SuggestedUserId, c.SuggestedCardId });
            modelBuilder.Entity<User>().HasMany(w => w.SuggestingSuggestions).WithRequired(w => w.SuggestingUser).WillCascadeOnDelete(false);
            modelBuilder.Entity<User>().HasMany(w => w.SuggestedSuggestions).WithRequired(w => w.SuggestedUser).WillCascadeOnDelete(false);

        }
    }
}
