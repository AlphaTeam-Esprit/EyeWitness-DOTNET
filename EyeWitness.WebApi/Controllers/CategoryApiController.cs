﻿using EyeWitness.Data;
using EyeWitness.Domain.Entities;
using EyeWitness.WebApi.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Http;
using System.Web.Http;
using System.Web.Http.Description;

namespace EyeWitness.WebApi.Controllers
{
    public class CategoryApiController : ApiController
    {
        // GET: api/CategoryApi
        public IQueryable<CategoryModel> Get()
        {
            List<CategoryModel> list = new List<CategoryModel>();

            IEnumerable<Category> cats = new EyeWitnessContext().Category.AsEnumerable();
            foreach (Category c in cats)
            {
                list.Add(new CategoryModel() { Id = c.Id, name = c.name, Status = c.Status});
            }
            
            return list.AsQueryable();
        }

        // GET: api/CategoryApi/5
        [ResponseType(typeof(CategoryModel))]
        public IHttpActionResult Get(int id)
        {
            return Ok(new EyeWitnessContext().Category.Find(id));
        }

        // POST: api/CategoryApi
        public void Post([FromBody]string value)
        {
        }

        // PUT: api/CategoryApi/5
        public void Put(int id, [FromBody]string value)
        {
        }

        // DELETE: api/CategoryApi/5
        public void Delete(int id)
        {
        }
    }
}
