﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EyeWitness.WebApi.Models
{
    public class CategoryModel
    {
        public int Id { get; set; }
        public string name { get; set; }
        public String Status { get; set; }
    }
}