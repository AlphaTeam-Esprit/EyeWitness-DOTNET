﻿using System;
using System.Collections.Generic;
using System.Linq;
using Microsoft.Owin;
using Owin;
using System.Data.Entity;
using MySql.Data.Entity;

[assembly: OwinStartup(typeof(EyeWitness.WebApi.Startup))]

namespace EyeWitness.WebApi
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            //DbConfiguration.SetConfiguration(new MySqlEFConfiguration());
        }
    }
}
