﻿using EyeWitness.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EyeWitness.Data.Infrastructure;

namespace EyeWitness.Services.Repositories
{
    public class ChoosenCriteriasServices : Service<ChoosenCriterias>
    {

        public ChoosenCriteriasServices() : base(new UnitOfWork(new DatabaseFactory()))
        {
        }

        public void VoteForCriteria(int categoryId, int criteriaId)
        {
            ChoosenCriterias cc = Get(c => c.category.Id.Equals(categoryId) && c.criterias.Id.Equals(criteriaId));
            cc.vote += 1;
            Update(cc);
            Commit();
        }

        public IEnumerable<ChoosenCriterias> GetCriteriasByCategoryId(int categoryId)
        {
            return GetMany(c => c.CategoryId == categoryId);
        }

        public int GetTotalVotes()
        {
            return GetAll().Select(c => c.vote).Sum();
        }
    }
}
