﻿using EyeWitness.Data.Infrastructure;
using EyeWitness.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Services.Repositories
{
    public class SubscribeService : Service<SubscriptionsWC>
    {   

        public SubscribeService() : base(new UnitOfWork(new DatabaseFactory()))
        {

        }

        public void subscribeToWitnessCard(int idWc, int idUser)
        {
            this.Add(new SubscriptionsWC
            {   Type="t",
                UserId=idUser ,
                WitnessCardId =idWc,
                subscriptionDate =DateTime.Now
            });

        }
        public void unsubscribeToWitnessCard(int  idwWitnessCard, int idUser)
        {
            this.Delete(this.Get(s=>s.UserId==idUser && s.WitnessCardId==idwWitnessCard));

        }

       
        public String isSubscribed(WitnessCard wc, User u)
        {
            if (this.GetMany(s => s.UserId == u.Id && s.WitnessCardId==wc.Id).Count()==0)
                return "false";
            else
                return "true";
        }
        public void sendConfirmationByMail(int idUser, string body, int idwc)
        {
            WitnessCard wc = new RepositoryBase<WitnessCard>(new DatabaseFactory()).GetById(idwc);
            SmtpClient smtp = new SmtpClient();
            MailMessage msg = new MailMessage();
            smtp.Credentials = new NetworkCredential("othmen.boutaba@esprit.tn", "71970735");
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Timeout = 20000;
            msg.From = new MailAddress("othmen.boutaba@esprit.tn");
            msg.To.Add("othmen.boutaba@gmail.com");
            msg.Body = body+" "+wc.Id+" WitnessCard";
            smtp.Send(msg);
        }
    }
}
