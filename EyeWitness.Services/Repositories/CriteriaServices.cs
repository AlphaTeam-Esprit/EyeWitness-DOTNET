﻿using EyeWitness.Data.Infrastructure;
using EyeWitness.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Services.Repositories
{
    public class CriteriaServices : Service<Criterias>
    {
        public CriteriaServices() : base(new UnitOfWork(new DatabaseFactory()))
        {

        }

        public string GetCriteriaNameById(int criteriaId)
        {
            return GetById(criteriaId).name;
        }
    }
}
