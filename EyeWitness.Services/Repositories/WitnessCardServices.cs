﻿using EyeWitness.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EyeWitness.Data.Infrastructure;
using EyeWitness.Domain.SearchCriterias;
using EyeWitness.Data;
using System.Net.Mail;
using System.Net;

namespace EyeWitness.Services.Repositories
{
    public class WitnessCardServices : Service<WitnessCard>
    {
        EyeWitnessContext context;
        public WitnessCardServices() : base(new UnitOfWork(new DatabaseFactory()))
        {
            context = new EyeWitnessContext();
        }
        
        public IEnumerable<WitnessCard> SearchWitnessCard(WitnessCardSearchData data)
        {
            IEnumerable<WitnessCard> res = GetAll().ToList();

            if (data.Address != null) res = res.Where(w => w.Address.Contains(data.Address));
            if (data.Latitude != 0) res = res.Where(w => data.Latitude.Equals(w.Latitude));
            if (data.Ambiences != null) res = res.Where(w => data.Ambiences.Contains(w.Ambience));
            if (data.Longitude != 0) res = res.Where(w => data.Longitude.Equals(w.Longitude));
            if (data.MeansOfPayment != null) res = res.Where(w => data.MeansOfPayment.Contains(w.MeansOfPayment));
            if (data.Name != null) res = res.Where(w => w.name.Contains(data.Name));
            if (data.OfferedServices != null) res = res.Where(w => data.OfferedServices.Contains(w.OfferedServices));
            if (data.CategoriesIds != null) res = res.Where(w => data.CategoriesIds.Contains(w.category.Id)).ToList();

            /* New Code */
            if (data.Latitude != 0 && data.Longitude != 0)
            {

            }
            /*          */

            switch (data.OrderBy)
            {
                case OrderByCriteria.NUMBER_OF_SUBSCRIBERS:
                    res = res.OrderBy(w => w.subscriptionsWC.Count);
                    break;
                case OrderByCriteria.RATING:
                    res = res.OrderBy(w => w.note);
                    break;
                case OrderByCriteria.ALPHABETICAL:
                default:
                    res = res.OrderBy(w => w.name);
                    break;
            }

            return res;
        }

        public IEnumerable<WitnessCard> SearchWitnessCard(WitnessCardSearchData data, int number)
        {
            return SearchWitnessCard(data, number, 1);
        }

        public IEnumerable<WitnessCard> SearchWitnessCard(WitnessCardSearchData data, int numberPerPage, int page)
        {
            IEnumerable<WitnessCard> res = GetAll();

            if (data.Address != null) res = res.Where(w => data.Address.Contains(w.Address));
            if (data.Latitude != 0) res = res.Where(w => data.Latitude.Equals(w.Latitude));
            if (data.Ambiences != null) res = res.Where(w => data.Ambiences.Contains(w.Ambience));
            if (data.Longitude != 0) res = res.Where(w => data.Longitude.Equals(w.Longitude));
            if (data.MeansOfPayment != null) res = res.Where(w => data.MeansOfPayment.Contains(w.MeansOfPayment));
            if (data.Name != null) res = res.Where(w => data.Name.Equals(w.name));
            if (data.OfferedServices != null) res = res.Where(w => data.OfferedServices.Contains(w.OfferedServices));
            if (data.CategoriesIds != null) res = res.Where(w => data.CategoriesIds.Contains(w.category.Id));

            switch (data.OrderBy)
            {
                case OrderByCriteria.NUMBER_OF_SUBSCRIBERS:
                    res = res.OrderBy(w => w.subscriptionsWC.Count);
                    break;
                case OrderByCriteria.RATING:
                    res = res.OrderBy(w => w.note);
                    break;
                case OrderByCriteria.ALPHABETICAL:
                default:
                    res = res.OrderBy(w => w.name);
                    break;
            }

            return res.Skip((page - 1) * numberPerPage).Take(numberPerPage);
        }

        public IEnumerable<WitnessCard> SearchWitnessCardByNameLike(String like, int number)
        {
            return GetMany(c => c.name.Contains(like)).Take(number);
        }

        public IEnumerable<WitnessCard> GetPopularWitnessCards(int number)
        {
            return GetMany(c => c.note >= 4).OrderBy(c => c.note).Take(number);
        }

        public void Suggest(SuggestionWC suggest)
        {
            IRepositoryBase<SuggestionWC> repo = GetUnitOfWork().getRepository<SuggestionWC>();
            repo.Add(suggest);



        }

        public IEnumerable<WitnessCard> GetUnpublishedCardByUserId(int userId)
        {
            return GetMany(c => c.creator.Id == userId);
        }

        public IEnumerable<WitnessCard> ListOfWCSuggested(int UserConnecteId)
        {

            return from m in context.SuggestionWCs
                   where m.SuggestedUserId == UserConnecteId
                   select m.SuggestedCard;
        }

        public void publishWC(int id)
        {

            WitnessCard wc = this.GetById(id);
            wc.status = "standBy";
            Update(wc);
            Commit();
            SmtpClient smtp = new SmtpClient();
            MailMessage msg = new MailMessage();
            smtp.Credentials = new NetworkCredential("ahmedmoalla@gmail.com", "ahmedmoalla");
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Timeout = 20000;
            msg.From = new MailAddress("ahmedmoalla@gmail.com");
            msg.To.Add("rim.aydi3@gmail.com");
            msg.Body = "J'attends votre validation";
            smtp.Send(msg);

        }

        public IEnumerable<WitnessCard> GetAllByCategoryId(int CategoryId)
        {
            
            List<WitnessCard> witcard = GetAll().Where(c => c.CategoryId == CategoryId).ToList();
            
            return witcard;

        }

        // TODO
        public IEnumerable<String> GetBestThreeCriteriasByCategory(int idcategory)
        {
            IEnumerable<ChoosenCriterias> ch = GetUnitOfWork().getRepository<ChoosenCriterias>().GetMany(t => t.CategoryId == idcategory).OrderByDescending(t => t.vote).Take(3);
            IEnumerable<String> criterias = ch.Select(t => t.criterias.name);
            return criterias;
        }


        public void AddWitnessCardNote(int note1, int note2, int note3, WitnessCard wc)
        {
            float Total = (float)(note1 + note2 + note3) / 3;
            wc.nbuser++;
            wc.note = (wc.note * (wc.nbuser - 1) + Total) / (wc.nbuser);
            wc.note = (float)Math.Round(wc.note, 2);
            this.Update(wc);
            this.Commit();

        }
        public IEnumerable<WitnessCard> GetBestTenWitnessCardsByCategoryId(int id)
        {
            return this.GetMany(t => t.CategoryId == id).OrderByDescending(y => y.note).Take(10);
        }
    }
}
