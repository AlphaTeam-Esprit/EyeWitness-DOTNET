﻿using EyeWitness.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EyeWitness.Data.Infrastructure;

namespace EyeWitness.Services.Repositories
{
    public class CategoryServices : Service<Category>
    {
        public CategoryServices() : base(new UnitOfWork(new DatabaseFactory()))
        {
        }

        public IEnumerable<Category> SearchCategoryByNameLike(String like, int number)
        {
            return GetMany(c => c.name.Contains(like)).Take(number);
        }

    }
}
