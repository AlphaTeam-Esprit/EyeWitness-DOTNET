﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using Service.Pattern;
using EyeWitness.Data.Infrastructure;
using EyeWitness.Domain.Entities;

namespace EyeWitness.Services.Repositories
{
    public class SuggestionService : Service<WitnessCard>
    {
        public SuggestionService() : base(new UnitOfWork(new DatabaseFactory()))
        {

        }
        public IEnumerable<WitnessCard> getUnavaliableWitnessCard(int idUser)
        {
            return this.GetMany(w => !(w.subscriptionsWC.Select(u => u.UserId).Contains(idUser)));
        }
        public List<int> NumberOfCategory()
        {
            return new RepositoryBase<Category>(new DatabaseFactory()).GetAll().Select(c=>c.Id).ToList(); 
        }
        public WitnessCard getBestWitnessCardByCategory(int idUser, int idCategory)
        {
            return getUnavaliableWitnessCard(idUser).Where(w => w.CategoryId == idCategory && w != null).OrderByDescending(c => c.note).Take(1).ToList()[0];
        }
        
        public List<WitnessCard> suggestWitnessCards(int idUser)
        {
            List<WitnessCard> suggWc = new List<WitnessCard>();
            Random rand = new Random();
            List<int> idCategories = NumberOfCategory();

            

            int cat1 = rand.Next(idCategories.Count);
            int cat2 = rand.Next(idCategories.Count);
            int cat3 = rand.Next(idCategories.Count);
            while (cat1 == cat2 || cat2 == cat3 || cat1 == cat3)
            {
                cat1 = rand.Next(idCategories.Count);
                cat2 = rand.Next(idCategories.Count);
                cat3 = rand.Next(idCategories.Count);
            }
            
            int idcat1 = idCategories[cat1];
            int idcat2 = idCategories[cat2];
            int idcat3 = idCategories[cat3];

            suggWc.Add(getBestWitnessCardByCategory(idUser, idcat1));
            suggWc.Add(getBestWitnessCardByCategory(idUser, idcat2));
            suggWc.Add(getBestWitnessCardByCategory(idUser, idcat3));

            return suggWc;
        }
    }
}
