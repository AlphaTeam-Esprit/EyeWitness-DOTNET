﻿using EyeWitness.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EyeWitness.Data.Infrastructure;

namespace EyeWitness.Services.Repositories
{
    public class EvaluationServices : Service<Evaluation>
    {

        UserServices userServices;
        public EvaluationServices() : base(new UnitOfWork(new DatabaseFactory()))
        {
            userServices = new UserServices();
        }

        public IEnumerable<Evaluation> GetEvaluationsOrderedByDate(WitnessCard card)
        {
            return card.evaluations.OrderBy(e => e.evaluationDate);
        }

        public IEnumerable<Evaluation> GetGoodEvaluations(WitnessCard card)
        {
            return card.evaluations.Where(e => e.rating > 3);
        }

        public IEnumerable<Evaluation> GetBadEvaluations(WitnessCard card)
        {
            return card.evaluations.Where(e => e.rating <= 3);
        }

        public IEnumerable<Evaluation> GetEvaluationsOrderedByRating(WitnessCard card)
        {
            return card.evaluations.OrderBy(e => e.rating);
        }

        public IEnumerable<Evaluation> GetPopularEvaluations(int number)
        {
            return GetAll().OrderBy(e => e.rating)/*.OrderBy(e => e.likes)*/.Take(number);
        }

        public IEnumerable<Evaluation> GetEvaluationsByUserId(int idUser)
        {
            return GetMany(x => x.user.Id == idUser).OrderBy(e => e.evaluationDate);
        }

        public IEnumerable<Evaluation> GetEvaluationsByWCId(int idWC)
        {
            return GetMany(x => x.WitnessCardId == idWC);
        }

        public IEnumerable<Evaluation> GetEvaluationsOfFollowedUsersByUserId(int iduser)
        {
            IEnumerable<User> followedUsers = userServices.GetFollowedUsers(iduser);
            List<Evaluation> evals = new List<Evaluation>();
            User userFound = userServices.GetById(iduser);
            foreach (User u in followedUsers)
            {
                evals.AddRange(this.GetMany().Where(y => y.UserId == u.Id));   
            }
            return evals.AsEnumerable();

        }
    }
}
