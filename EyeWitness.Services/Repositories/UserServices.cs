﻿using EyeWitness.Domain.Entities;
using Service.Pattern;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using EyeWitness.Data.Infrastructure;

namespace EyeWitness.Services.Repositories
{
    public class UserServices : Service<User>
    {
        WitnessCardServices witnessCardServices;

        public UserServices() : base(new UnitOfWork(new DatabaseFactory()))
        {
            witnessCardServices = new WitnessCardServices();
        }

        public IEnumerable<User> ListeUserByName(String searchString)
        {

            return GetUnitOfWork().getRepository<User>().GetMany(x => x.firstName.Contains(searchString));
        }



        public void followUserById(int idUserConnected, int idUserFollowed)
        {

            User UserFollowed = GetById((long)idUserFollowed);
            UserFollowed.followers.Add(GetById((long)idUserConnected));

            GetUnitOfWork().GetDbFactory().DataContext.User.Attach(UserFollowed);
            GetUnitOfWork().GetDbFactory().DataContext.Entry(UserFollowed).State = System.Data.Entity.EntityState.Modified;
            GetUnitOfWork().GetDbFactory().DataContext.SaveChanges();

        }

        public bool isFollower(User userfollowing, int idUserFollowed)
        {
            User UserFollowed = GetById((long)idUserFollowed);
            if (UserFollowed.followers.Contains(userfollowing))
            {

                return true;
            }

            return false;
        }



        public string becomeAgent(int idConnectedUser, int idWC)
        {
            User connectedUser = GetById((long)idConnectedUser);
            connectedUser.status = "standBy";
            Update(connectedUser);
            Commit();

            WitnessCard concernedWitnessCArd = witnessCardServices.GetById((long)idWC);


            // send the mail to the administrator 

            String MyAgentRequest = "hi Iam" + connectedUser.firstName + "" + connectedUser.lastName + "and I want to get the privillege  of beeing agent on the following witness card" +
                 " \n  name : " + concernedWitnessCArd.name +
                 " \n  category : " + concernedWitnessCArd.category.name;



            return MyAgentRequest;

        }


        public IEnumerable<User> GetFollowedUsers(int iduser)
        {

            IEnumerable<User> followedUsers = new List<User>();

            User foundUser = GetById(iduser);
            IEnumerable<User> users = GetAll().ToList();
            followedUsers = from u in users
                            where u.followers.Contains(foundUser)
                            select u; 
                            
            return followedUsers;

        }

        public User getUnfollowedUser(int idUser, int idOtherUser)
        {
            return this.GetMany(u => !u.followers.Contains(this.GetById(idOtherUser)) && u.Id == idUser).First();
        }

        public bool isFollowed(int idUser, int idOtherUser)
        {
            User otherUser = GetById(idOtherUser);
            User thisUser = GetById(idUser);

            return otherUser.followers.Contains(thisUser);

        }

        public IEnumerable<WitnessCard> getWitnessCardsByUserId(int idUser)
        {
            return GetById(idUser).witnessCardCreated;
        }

        public IEnumerable<WitnessCard> getSubscribedWitnessCardByUserId(int idUser)
        {
            WitnessCardServices witnessCardServices = new WitnessCardServices();
            return witnessCardServices.GetMany(w => (w.subscriptionsWC.Select(u => u.UserId).Contains(idUser)));
        }
    }
}
