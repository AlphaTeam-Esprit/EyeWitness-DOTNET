﻿using EyeWitness.Domain.Entities;
using EyeWitness.GUI.Identity.Models;
using EyeWitness.Services.Repositories;
using EyeWitness.Domain.SearchCriterias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EyeWitness.GUI.Identity.Helpers;

namespace EyeWitness.GUI.Identity.Controllers
{
    public class SearchController : Controller
    {

        private CategoryServices categoryService;
        private WitnessCardServices witnessCardService;

        public SearchController()
        {
            categoryService = new CategoryServices();
            witnessCardService = new WitnessCardServices();
        }

        // GET: Search
        public ActionResult Index(int page = 1)
        {
            
            SearchViewModel model;
            if (Session["SearchData"] != null)
            {
                // Setup page
                model = Session["SearchData"] as SearchViewModel;
                model.CurrentPage = page;
                return View(model);
            }

            model = new SearchViewModel();
            model.Ambiences = Enum.GetValues(typeof(AmbienceCriteria))
               .Cast<AmbienceCriteria>()
               .Select(a => new SelectListItem()
               {
                   Text = a.ToString(),
                   Value = a.ToString(),
                   Selected = false
               });

            model.Categories = categoryService.GetAll()
              .Select(c => new SelectListItem()
              {
                  Text = c.name,
                  Value = c.Id.ToString(),
                  Selected = false
              });

            model.MeansOfPayment = Enum.GetValues(typeof(MeansOfPaymentCriteria))
               .Cast<MeansOfPaymentCriteria>()
               .Select(a => new SelectListItem()
               {
                   Text = a.ToString(),
                   Value = a.ToString(),
                   Selected = false
               });

            model.OfferedServices = Enum.GetValues(typeof(OfferedServicesCriteria))
               .Cast<OfferedServicesCriteria>()
               .Select(a => new SelectListItem()
               {
                   Text = a.ToString(),
                   Value = a.ToString(),
                   Selected = false
               });

            model.OrderByItems = Enum.GetValues(typeof(OrderByCriteria))
               .Cast<OrderByCriteria>()
               .Select(a => new SelectListItem()
               {
                   Text = a.ToString(),
                   Value = a.ToString(),
                   Selected = false
               });

            model.Name = model.Latitude = model.Longitude = model.Address = "";
            model.NearMe = false;
            model.OrderByItems = Enum.GetValues(typeof(OrderByCriteria))
               .Cast<OrderByCriteria>()
               .Select(a => new SelectListItem()
               {
                   Text = a.ToString(),
                   Value = a.ToString(),
                   Selected = false
               });
            model.OrderBy = OrderByCriteria.ALPHABETICAL.ToString();
            //IEnumerable<WitnessCard> wcs = witnessCardService.GetPopularWitnessCards(6);
            model.SearchResults = witnessCardService.GetPopularWitnessCards(6).ToViewModelEnumerable();

            return View(model);
        }

        [HttpPost]
        public ActionResult Search(FormCollection form)
        {
            SearchViewModel model = new SearchViewModel();
            WitnessCardSearchData data = new WitnessCardSearchData();

            model.Name = form.Get("Name");
            data.Name = model.Name == "" ? null : model.Name;
            if (form.Get("Ambiences") == null)
            {
                data.Ambiences = null;
                model.Ambiences = Enum.GetValues(typeof(AmbienceCriteria))
               .Cast<AmbienceCriteria>()
               .Select(a => new SelectListItem()
               {
                   Text = a.ToString(),
                   Value = a.ToString(),
                   Selected = false
               });
            }
            else
                model.Ambiences = GetEnumSelectListItemFromForm("Ambiences", typeof(AmbienceCriteria), form, data);

            if (form.Get("OfferedServices") == null)
            {
                data.OfferedServices = null;
                model.OfferedServices = Enum.GetValues(typeof(OfferedServicesCriteria))
               .Cast<OfferedServicesCriteria>()
               .Select(a => new SelectListItem()
               {
                   Text = a.ToString(),
                   Value = a.ToString(),
                   Selected = false
               });
            }
            else
                model.OfferedServices = GetEnumSelectListItemFromForm("OfferedServices", typeof(OfferedServicesCriteria), form, data);

            if (form.Get("MeansOfPayment") == null)
            {
                data.MeansOfPayment = null;
                model.MeansOfPayment = Enum.GetValues(typeof(MeansOfPaymentCriteria))
               .Cast<MeansOfPaymentCriteria>()
               .Select(a => new SelectListItem()
               {
                   Text = a.ToString(),
                   Value = a.ToString(),
                   Selected = false
               });
            }
            else
                model.MeansOfPayment = GetEnumSelectListItemFromForm("MeansOfPayment", typeof(MeansOfPaymentCriteria), form, data);

            if (form.Get("Categories") == null)
            {
                data.CategoriesIds = null;
                model.Categories = categoryService.GetAll()
              .Select(c => new SelectListItem()
              {
                  Text = c.name,
                  Value = c.Id.ToString(),
                  Selected = false
              });
            }
            else
                model.Categories = GetCategoriesFromForm(form, data);

            float parseResult;
            model.Longitude = form.Get("Longitude");
            data.Longitude = float.TryParse(model.Longitude, out parseResult) ? parseResult : 0;

            model.Latitude = form.Get("Latitude");
            data.Latitude = float.TryParse(model.Latitude, out parseResult) ? parseResult : 0;

            model.Address = form.Get("Address");
            data.Address = model.Address == "" ? null : model.Address;

            model.NearMe = false;
            data.NearMe = model.NearMe;

            model.OrderByItems = Enum.GetValues(typeof(OrderByCriteria))
               .Cast<OrderByCriteria>()
               .Select(a => new SelectListItem()
               {
                   Text = a.ToString(),
                   Value = a.ToString(),
                   Selected = false
               });
            model.OrderBy = form.Get("OrderBy");
            OrderByCriteria enumParseResult;
            data.OrderBy = Enum.TryParse(model.OrderBy, out enumParseResult) ? enumParseResult : OrderByCriteria.ALPHABETICAL;

            model.SearchResults = witnessCardService.SearchWitnessCard(data).ToViewModelEnumerable();
            ViewBag.nb = "Count: " + model.SearchResults.Count();


            Session["SearchData"] = data.IsNull() ? null : model;
            return RedirectToAction("Index");
        }

        private IEnumerable<SelectListItem> GetCategoriesFromForm(FormCollection form, WitnessCardSearchData data)
        {

            List<SelectListItem> l = new List<SelectListItem>();

            string[] valuesSplitted = form.Get("Categories").Split(',').ToArray();
            List<Category> cats = new List<Category>();

            List<int> dataCategoryIds = new List<int>();

            // Adding selected items
            foreach (string i in valuesSplitted)
            {
                dataCategoryIds.Add(int.Parse(i));
                cats.Add(categoryService.GetById(int.Parse(i)));
            }

            data.CategoriesIds = dataCategoryIds.Count == 0 ? null : dataCategoryIds;

            foreach (Category c in cats)
            {
                l.Add(new SelectListItem()
                {
                    Text = c.name,
                    Value = c.Id.ToString(),
                    Selected = true
                });
            }

            // Adding unselected items
            List<Category> unselectedCats = categoryService.GetMany(c => !valuesSplitted.Contains(c.Id.ToString())).ToList();

            foreach (Category c in unselectedCats)
            {
                l.Add(new SelectListItem()
                {
                    Text = c.name,
                    Value = c.Id.ToString(),
                    Selected = false
                });
            }


            return l;
        }

        private IEnumerable<SelectListItem> GetEnumSelectListItemFromForm(String key, Type typeOfEnum, FormCollection form, WitnessCardSearchData data)
        {
            string values = form.Get(key);

            string[] valuesSplitted = values.Split(',');

            List<SelectListItem> l = new List<SelectListItem>();

            List<string> dataAmbience = new List<string>();
            List<string> dataPayment = new List<string>();
            List<string> dataServices = new List<string>();

            // Adding selected items
            foreach (string s in valuesSplitted)
            {
                if (typeOfEnum.Equals(typeof(AmbienceCriteria))) dataAmbience.Add(s);
                else if (typeOfEnum.Equals(typeof(MeansOfPaymentCriteria))) dataPayment.Add(s);
                else if (typeOfEnum.Equals(typeof(OfferedServicesCriteria))) dataServices.Add(s);

                l.Add(new SelectListItem()
                {
                    Text = s,
                    Value = s,
                    Selected = true
                });
            }

            data.Ambiences = dataAmbience.Count == 0 ? null : dataAmbience;
            data.MeansOfPayment = dataPayment.Count == 0 ? null : dataPayment;
            data.OfferedServices = dataServices.Count == 0 ? null : dataServices;

            Array enumValues = Enum.GetValues(typeOfEnum);
            string[] strs = new string[enumValues.Length];

            for (int i = 0; i < enumValues.Length; i++)
            {
                strs[i] = enumValues.GetValue(i).ToString();
            }
            // Adding Unselected items
            List<string> unselectedItems = strs.Where(a => !valuesSplitted.Contains(a)).ToList();

            foreach (string s in unselectedItems)
            {
                l.Add(new SelectListItem()
                {
                    Text = s,
                    Value = s,
                    Selected = false
                });
            }
            return l;
        }
    }
}
