﻿using EyeWitness.Data;
using EyeWitness.Domain.Entities;
using EyeWitness.Domain.SearchCriterias;
using EyeWitness.GUI.Identity.Helpers;
using EyeWitness.GUI.Identity.Models;
using EyeWitness.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;
using System.Data.Entity.Migrations;
using System.Data.Entity;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;

namespace EyeWitness.GUI.Identity.Controllers
{
    public class HomeController : Controller
    {
        WitnessCardServices witnessCardServices;
        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public HomeController()
        {
            witnessCardServices = new WitnessCardServices();
        }

        public PartialViewResult Chat()
        {
            ChatViewModel model = new ChatViewModel();
            model.Followers = new List<ChatUser>();
            
            User user = UserManager.FindById(User.Identity.GetUserId()).UserInfo;

            model.LoggedUserId = user.Id;

            ICollection<User> followers = user.followers;

            foreach (User u in followers)
            {
                model.Followers.Add(new ChatUser()
                {
                    Avatar = u.avatar,
                    FullName = u.firstName + " " + u.lastName,
                    Id = u.Id
                });
            }

            return PartialView("Chat", model);
        }

        public ActionResult Index()
        {
            /*
            WitnessCardServices svc = new WitnessCardServices();
            
            EyeWitnessContext ctx = new EyeWitnessContext();
            ctx.Category.Add(new Domain.Entities.Category()
            {
                name = "My CAT",
                Status = "stat"
            });
            ctx.SaveChanges();*/
            HomePageViewModel model = new HomePageViewModel();
            model.Name = "Model Name";
            model.PopularWitnessCards = witnessCardServices.GetPopularWitnessCards(6).ToViewModelEnumerable();

            return View(model);
        }

        public ActionResult Cat()
        {

            EyeWitnessContext ctx = new EyeWitnessContext();

            ViewBag.var = "Count : " + ctx.WitnessCard.Where(c => c.name.Contains("Name")).Count();
            return View(ctx.Category.Find(85));
        }

        public ActionResult About()
        {
            ViewBag.Message = "Your application description page.";

            return View();
        }

        public ActionResult Contact()
        {
            ViewBag.Message = "Your contact page.";

            return View();
        }

        [HttpPost]
        public ActionResult ContactUs(FormCollection form)
        {

            SmtpClient smtp = new SmtpClient();
            MailMessage msg = new MailMessage();
            smtp.Credentials = new NetworkCredential("ahmed.moalla@gmail.com", "ahmedmoalla");
            smtp.Host = "smtp.gmail.com";
            smtp.Port = 587;
            smtp.EnableSsl = true;
            smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
            smtp.Timeout = 20000;
            msg.From = new MailAddress("ahmed.moalla@gmail.com", form.Get("username"));
            msg.To.Add("ahmed.moalla@gmail.com");
            msg.Subject = "From : " + form.Get("email") + " => " + form.Get("subject");
            msg.Body = form.Get("message");
            smtp.Send(msg);

            return RedirectToAction("Index");
        }

        public ActionResult MockData()
        {
            int nbCards = 20,
                nbCategories = 5,
                nbSubscriptions = 5;
            EyeWitnessContext ctx = new EyeWitnessContext();

            Random rand = new Random();
            // Categories

            for (int i = 0; i < nbCategories; i++)
            {
                Category cat = new Category()
                {
                    Id = i + 1,
                    name = "Category " + i
                };
                ctx.Category.AddOrUpdate(cat);
            }
            ctx.SaveChanges();

            // Criterias
            for (int i = 0; i < 10; i++)
            {

                Criterias crit = new Criterias()
                {
                    Id = i + 1,
                    name = "Criteria " + i
                };

                ctx.Criterias.AddOrUpdate(crit);
            }
            ctx.SaveChanges();

            // Choosen Criterias
            for (int i = 0; i < nbCategories; i++)
            {

                for (int j = 0; j < 10; j++)
                {
                    ChoosenCriterias cc = new ChoosenCriterias()
                    {
                        CategoryId = i + 1,
                        CriteriasId = j + 1,
                        vote = rand.Next(250)
                    };
                    ctx.ChoosenCriterias.AddOrUpdate(cc);
                }

            }
            ctx.SaveChanges();

            //Users
            ctx.User.AddOrUpdate(
                new User() {
                    Id = 1,
                    firstName = "Ahmed",
                    lastName = "Moalla",
                    address = "Rue de Tunis",
                    avatar = "ahmed_moalla.jpg",
                    birthDate = DateTime.Now,
                    email = "ahmed.moalla@yahoo.fr",
                },
                new User()
                {
                    Id = 2,
                    firstName = "Dhiaeddine",
                    lastName = "Eladib", 
                    address = "Rue de France",
                    avatar = "dhiaeddine_eladib.jpg",
                    birthDate = DateTime.Now + TimeSpan.FromDays(100),
                    email = "dhiaeddine.eladib@yahoo.fr"
                },
                new User()
                {
                    Id = 3,
                    firstName = "Khaled",
                    lastName = "Ben Marzouk",
                    address = "Rue d'allemagne",
                    avatar = "khaled_ben_marzouk.jpg",
                    birthDate = DateTime.Now - TimeSpan.FromDays(150),
                    email = "khaled.benmarzouk@yahoo.fr"
                }
            );

            ctx.SaveChanges();

            // Followers
            try { 
                User user1 = ctx.User.Find(1);
                User user2 = ctx.User.Find(2);
                User user3 = ctx.User.Find(3);
                user1.followers.Add(user2);
                user1.followers.Add(user3);
                user2.followers.Add(user1);
                user2.followers.Add(user3);
                user3.followers.Add(user1);

                ctx.User.Attach(user1);
                ctx.User.Attach(user2);
                ctx.User.Attach(user3);
                ctx.Entry(user1).State = EntityState.Modified;
                ctx.Entry(user2).State = EntityState.Modified;
                ctx.Entry(user3).State = EntityState.Modified;

                ctx.SaveChanges();
            }catch
            {
                // not adding if pk constraint error
            }
            // Witness Cards
            for (int i = 0; i < nbCards; i++)
            {
                WitnessCard wc = new WitnessCard()
                {
                    Id = i + 1,
                    name = "Name " + i,
                    Address = "Random address " + i,
                    Ambience = GetRandomEnumName(typeof(AmbienceCriteria), rand),
                    CategoryId = rand.Next(1, 6),
                    agentId = rand.Next(1, 4),
                    creatorId = rand.Next(1, 4),
                    description = GetLoremIpsum(),
                    Latitude = (float)rand.NextDouble() * 1000,
                    Longitude = (float)rand.NextDouble() * 1000,
                    note = rand.Next(6),
                    MeansOfPayment = GetRandomEnumName(typeof(MeansOfPaymentCriteria), rand),
                    OfferedServices = GetRandomEnumName(typeof(OfferedServicesCriteria), rand),
                    picture = "~/Content/_Uploads/WitnessCards/" + rand.Next(1, 7) + ".png"
                };

                ctx.WitnessCard.AddOrUpdate(wc);
            }
            ctx.SaveChanges();

            // Subscribed cards
            for (int i = 0; i < nbSubscriptions; i++)
            {
                SubscriptionsWC sub = new SubscriptionsWC()
                {
                    subscriptionDate = DateTime.Now - TimeSpan.FromDays(rand.Next(150)),
                    UserId = rand.Next(1, 4),
                    WitnessCardId = rand.Next(1, nbCards)
                };
                ctx.SubscriptionsWC.AddOrUpdate(sub);
            }
            ctx.SaveChanges();

            return RedirectToAction("Index");
        }

        private Category GetRandomCategory(EyeWitnessContext ctx, Random rand)
        {
            var skip = (int)(rand.NextDouble() * ctx.Category.Count());
            return ctx.Category.OrderBy(c => c.Id).Skip(skip).Take(1).First();
        }

        private string GetRandomEnumName(Type t, Random rand)
        {
            return Enum.GetNames(t)[rand.Next(0, Enum.GetNames(t).Length)];
        }

        private string GetLoremIpsum()
        {
            return "Sed ut perspiciatis unde omnis iste natus error sit voluptatem accusantium doloremque laudantium, totam rem aperiam, eaque ipsa quae ab illo inventore veritatis et quasi architecto beatae vitae dicta sunt explicabo.";
        }
    }
}