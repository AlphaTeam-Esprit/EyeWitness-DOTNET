﻿using EyeWitness.Domain.Entities;
using EyeWitness.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;
using EyeWitness.GUI.Identity.Helpers;
using EyeWitness.GUI.Identity.Models;
using System.IO;
using Microsoft.AspNet.Identity.Owin;
using Microsoft.AspNet.Identity;
using System.Net.Mail;

namespace EyeWitness.GUI.Identity.Controllers
{
    public class EvaluationController : Controller
    {
        EvaluationServices evaluationServices;
        WitnessCardServices witnessCardServices;

        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public EvaluationController()
        {
            evaluationServices = new EvaluationServices();
            witnessCardServices = new WitnessCardServices();
        }

        // GET: Evaluation/Index/5
        public ActionResult Index(int id)
        {
            WitnessCard witnessCardFound = witnessCardServices.GetById(id);
            IEnumerable<Evaluation> evals = evaluationServices.GetEvaluationsOrderedByDate(witnessCardFound);

            return View(evals.ToViewModelEnumerable());
        }


        public ActionResult Create(int id)
        {
            EvaluationsViewModel model = new EvaluationsViewModel();
            model.WitnessCardId = id;
            model.WitnessCard = witnessCardServices.GetById(id);
            model.UserImageUrl = "/Content/_Uploads/TeamMembers/ahmed_moalla.jpg";
            model.UserFirstName = "Ahmed";
            model.UserLastName = "Agent";
            return View(model);
        }

        [HttpPost]
        public ActionResult Create(EvaluationsViewModel model)
        {

            if (ViewData.ModelState.IsValid)
            {
                var video = Request.Files.Get(0);
                string picturesUrl = "";
                string videoUrl = video.FileName;


                for (int i = 1; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase img = Request.Files[i];
                    picturesUrl += img.FileName + Evaluation.PICTURE_SEPARATOR;
                }

                Evaluation eval = new Evaluation()
                {
                    evaluationDate = DateTime.Now,
                    comment = model.Description,
                    rating = model.Rating,
                    UserId = 1,
                    WitnessCardId = model.WitnessCardId,
                    video = videoUrl,
                    pictures = picturesUrl
                };

                evaluationServices.Add(eval);
                evaluationServices.Commit();

                //Saving images
                for (int i = 1; i < Request.Files.Count; i++)
                {
                    HttpPostedFileBase img = Request.Files[i];
                    var path = Path.Combine(Server.MapPath("~/Content/_Uploads/Images"), img.FileName);
                    img.SaveAs(path);
                }

                // Saving video
                video.SaveAs(Path.Combine(Server.MapPath("~/Content/_Uploads/Videos"), video.FileName));


                return RedirectToAction("Index", new { id = model.WitnessCardId });
            }

            return View();
        }

        public ActionResult MyEvaluation()
        {
            ApplicationUserManager UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            int idConnectedUSer = UserManager.FindById(User.Identity.GetUserId()).UserInfo.Id;

            List<MyEvaluationsModel> ListEvaluations = new List<MyEvaluationsModel>();
            var evaluationList = evaluationServices.GetEvaluationsByUserId(idConnectedUSer);
            foreach (var item in evaluationList)
            {
                ListEvaluations.Add(new MyEvaluationsModel

                {

                    evaluationDate = item.evaluationDate,
                    WCname = (witnessCardServices.GetById((long)item.WitnessCardId)).name,
                    WCDescription = (witnessCardServices.GetById((long)item.WitnessCardId)).description,

                });

            }
            return View(ListEvaluations);
        }

        // GET: Evaluation
        public ActionResult FollowedEvaluations()
        {
            User currentUser = UserManager.FindById(User.Identity.GetUserId()).UserInfo;

            List<EvaluationViewModel> evaluationviewmodel = new List<EvaluationViewModel>();
            List<Evaluation> evaluations = evaluationServices.GetEvaluationsOfFollowedUsersByUserId(currentUser.Id).ToList();
            foreach (Evaluation eval in evaluations)
            {
                evaluationviewmodel.Add(
                     new EvaluationViewModel()
                     {
                         rating = eval.rating,
                         picture = eval.GetPicturesUrls()[0],
                         video = eval.video,
                         comment = eval.comment,
                         evaluationDate = eval.evaluationDate
                     });
            }

            return View(evaluationviewmodel);
        }


    }
}
