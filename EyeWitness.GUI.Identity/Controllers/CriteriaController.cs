﻿using EyeWitness.Domain.Entities;
using EyeWitness.GUI.Identity.Models;
using EyeWitness.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EyeWitness.GUI.Identity.Controllers
{
    public class CriteriaController : Controller
    {
        CategoryServices categoryServices;
        ChoosenCriteriasServices choosenCriteriaServices;
        CriteriaServices criteriaServices;

        public CriteriaController()
        {
            categoryServices = new CategoryServices();
            choosenCriteriaServices = new ChoosenCriteriasServices();
            criteriaServices = new CriteriaServices();
        }
        //id Category
        public ActionResult Vote(int id)
        {
            CriteriaVoteViewModel model = new CriteriaVoteViewModel();

            IEnumerable<ChoosenCriterias> crits = choosenCriteriaServices.GetCriteriasByCategoryId(id);

            List<CriteriaViewModel> list = new List<CriteriaViewModel>();
            int totalVotes = choosenCriteriaServices.GetTotalVotes();
            
            foreach(ChoosenCriterias c in crits)
            {
                Criterias crit = criteriaServices.GetById(c.CriteriasId);
                list.Add(new CriteriaViewModel()
                {
                    Id = crit.Id,
                    Name = crit.name,
                    CurrentVotes = c.vote,
                    PercentTotalVotes = c.vote * 100 / totalVotes
                });
            }

            model.CriteriaModels = list;
            model.Category = categoryServices.GetById(id);
            model.TotalVotes = totalVotes;

            return View(model);
        }

        public string VoteForCriteria(int catId, int critId, int critId2, int critId3)
        {
            try
            {
                choosenCriteriaServices.VoteForCriteria(catId, critId);
                choosenCriteriaServices.VoteForCriteria(catId, critId2);
                choosenCriteriaServices.VoteForCriteria(catId, critId3);
                return "OK";
            } catch(Exception e)
            {
                return e.ToString();
            }
            
        }

    }
}
