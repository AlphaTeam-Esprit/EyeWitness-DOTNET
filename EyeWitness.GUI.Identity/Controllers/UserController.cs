﻿using EyeWitness.Domain.Entities;
using EyeWitness.GUI.Identity.Models;
using EyeWitness.Services.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace EyeWitness.GUI.Identity.Controllers
{
    public class UserController : Controller
    {
        UserServices userServices;

        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public UserController()
        {
            userServices = new UserServices();
        }

        public ActionResult FollowUser(int idUserFollowed)
        {
            ApplicationUserManager UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            User connectedUser = UserManager.FindById(User.Identity.GetUserId()).UserInfo;

            bool isFollower = userServices.isFollower(connectedUser, idUserFollowed);

            ViewBag.isFollower = isFollower;
            if (!isFollower)
            {
                userServices.followUserById(connectedUser.Id, idUserFollowed);
            }

            // this redirection does not work deman !
            return RedirectToAction("searchUser");
        }

        public ActionResult searchUser()
        {
            ViewBag.Message = "searching user by name";

            return View();
        }


        [HttpPost]
        public ActionResult searchUser(string searchString)
        {

            List<UserModel> ListUserModel = new List<UserModel>();

            if (!String.IsNullOrEmpty(searchString))
            {
                var ListUsers = userServices.ListeUserByName(searchString);

                foreach (var item in ListUsers)
                {
                    ListUserModel.Add(new UserModel

                    {
                        firstName = item.firstName,
                        lastName = item.lastName,
                        Id = item.Id,
                        avatar = item.avatar
                    });

                }


            }
            return View(ListUserModel);
        }


        public ActionResult becomeAgent(int idWC)
        {
            ApplicationUserManager UserManager = HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            int idConnectedUser = UserManager.FindById(User.Identity.GetUserId()).UserInfo.Id;
            //update the user status 
            String mailbody = userServices.becomeAgent(idConnectedUser, idWC);

            try
            {
                MailMessage message = new MailMessage("eyewitnessagent@gmail.com", "alaeddinemhamed@gmail.com", "AgentRequest", mailbody);

                SmtpClient client = new SmtpClient("smtp.gmail.com", 587);
                client.EnableSsl = true;
                client.DeliveryMethod = SmtpDeliveryMethod.Network;

                client.Credentials = new System.Net.NetworkCredential("eyewitnessagent@gmail.com", "eyewitness.com");



                client.Send(message);

            }
            catch (Exception ex)
            {
                ViewBag.Message = "Exception in sendEmail:" + ex.Message;
            }




            return View();

        }
        /*
        // GET: Profil/Create
        public ActionResult UserProfile()
        {
            ProfilViewModel profilv = new ProfilViewModel();

            return View(profilv);
        }*/
        /*
        // POST: Profil/Create
        [HttpPost]
        public ActionResult UpdateProfile(FormCollection collection, ProfilViewModel profilviewmodel, HttpPostedFileBase Image)
        {
            if (!ModelState.IsValid || Image == null || Image.ContentLength == 0)
            {
                RedirectToAction("Create");

            }

            profilviewmodel.avatar = Image.FileName;
            User user = new User
            {
                address = profilviewmodel.address,
                avatar = profilviewmodel.avatar,
                email = profilviewmodel.email,
                firstName = profilviewmodel.firstName,
                birthDate = profilviewmodel.birthDate,

                lastName = profilviewmodel.lastName,
                password = profilviewmodel.password,
                phoneNumber = profilviewmodel.phoneNumber,
                //  role = "taxi",
                // status = "agent",
                noteXP = 5

            };

            userServices.Add(user);
            userServices.Commit();
            var path = Path.Combine(Server.MapPath("~/Content/_Uploads/Avatars/"), Image.FileName);
            Image.SaveAs(path);
            return RedirectToAction("UserProfile");

        }
        */
        // GET: Profil/Edit/5
        public ActionResult UserProfile()
        {
            User currentUser = UserManager.FindById(User.Identity.GetUserId()).UserInfo;
            ProfilViewModel profilviewmodel = new ProfilViewModel();
            profilviewmodel.address = currentUser.address;
            profilviewmodel.birthDate = currentUser.birthDate;
            profilviewmodel.email = currentUser.email;
            profilviewmodel.firstName = currentUser.firstName;
            profilviewmodel.lastName = currentUser.lastName;
            profilviewmodel.phoneNumber = currentUser.phoneNumber;
            profilviewmodel.password = currentUser.password;
            profilviewmodel.avatar = currentUser.avatar;
            return View(profilviewmodel);
        }

        // POST: Profil/Edit/5
        [HttpPost]
        public ActionResult UpdateUserProfile(IndexViewModel model)
        {
            ApplicationUser currentApplicationUser = UserManager.FindById(User.Identity.GetUserId());
            User currentUser = currentApplicationUser.UserInfo;

            currentUser.address = model.Address;
            currentUser.birthDate = model.BirthDate;
            currentUser.email = model.Email;
            currentUser.firstName = model.FirstName;
            currentUser.lastName = model.LastName;
            currentUser.phoneNumber = model.PhoneNumber;

            currentApplicationUser.Email = model.Email;
            currentApplicationUser.UserName = model.Email;

            UserManager.Update(currentApplicationUser);

            return RedirectToAction("Index", "Manage");
        }

        // POST: Profil/Edit/5
        /*
        [HttpPost]
        public ActionResult UpdateUserProfile(FormCollection collection, ProfilViewModel profilviewmodel, HttpPostedFileBase Image)
        {
            //Controle on null
            profilviewmodel.avatar = Image.FileName;
            User currentUser = UserManager.FindById(User.Identity.GetUserId()).UserInfo;

            currentUser.address = profilviewmodel.address;
            currentUser.password = profilviewmodel.password;
            currentUser.birthDate = profilviewmodel.birthDate;
            currentUser.email = profilviewmodel.email;
            currentUser.firstName = profilviewmodel.email;
            currentUser.lastName = profilviewmodel.lastName;
            currentUser.phoneNumber = profilviewmodel.phoneNumber;
            currentUser.avatar = profilviewmodel.avatar;

            userServices.Update(currentUser);
            userServices.Commit();
            var path = Path.Combine(Server.MapPath("~/Content/Upload/"), Image.FileName);
            Image.SaveAs(path);

            return View();
        }*/

        // GET: UnfollowedUser
        // id : user to follow
        public ActionResult Index(int id)
        {

            User currentUser = UserManager.FindById(User.Identity.GetUserId()).UserInfo;
            User unfollowedUser = new User();
            IEnumerable<WitnessCard> createdWitnessCards = new List<WitnessCard>();
            IEnumerable<WitnessCard> subscribedWitnessCards = new List<WitnessCard>();
            if (!userServices.isFollowed(currentUser.Id, id))
            {
                unfollowedUser = userServices.GetById(id);
                subscribedWitnessCards = userServices.getSubscribedWitnessCardByUserId(unfollowedUser.Id);
                createdWitnessCards = userServices.getWitnessCardsByUserId(unfollowedUser.Id);

                ViewData["unfollowedUser"] = unfollowedUser;
                ViewData["subscribedWitnessCards"] = subscribedWitnessCards;
                ViewData["createdWitnessCardsEvaluation"] = createdWitnessCards.ToList().Select(w => w.evaluations != null).Take(2).Take(2);

            }
            else
            {
                //travail de khaled
            }
            return View();
        }
    }
}
