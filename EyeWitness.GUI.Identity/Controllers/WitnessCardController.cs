﻿using EyeWitness.Domain.Entities;
using EyeWitness.GUI.Identity.Helpers;
using EyeWitness.GUI.Identity.Models;
using EyeWitness.Services.Repositories;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using System;
using System.Collections.Generic;
using System.IO;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EyeWitness.GUI.Identity.Controllers
{
    public class WitnessCardController : Controller
    {
        WitnessCardServices witnessCardServices = null;
        CategoryServices categoryServices = null;
        SubscribeService subscribeService;
        SuggestionService suggestionService;

        public WitnessCardController()
        {
            witnessCardServices = new WitnessCardServices();
            categoryServices = new CategoryServices();
            subscribeService = new SubscribeService();
            suggestionService = new SuggestionService();
        }

        public ApplicationUserManager UserManager
        {
            get
            {
                return HttpContext.GetOwinContext().GetUserManager<ApplicationUserManager>();
            }
        }

        public ActionResult Index()
        {
            return View();
        }

        // GET: WitnessCard
        public ActionResult MyWitnessCards()
        {
            var witnessCard = witnessCardServices.GetUnpublishedCardByUserId(UserManager.FindById(User.Identity.GetUserId()).UserInfo.Id);
            List<WitnessCardModel> WCM = new List<WitnessCardModel>();
            foreach (var item in witnessCard)
            {
                WCM.Add(
                    new WitnessCardModel
                    {
                        name = item.name,
                        description = item.description,
                        picture = item.picture,
                        //CategoryId = item.CategoryId
                        Id = item.Id,
                        status = item.status
                    });
            }
            return View(WCM);
        }

        public ActionResult Suggest(int id)
        {
            SuggestModel WC = new SuggestModel()
            {
                IdWC = id

            };

            return View(WC);

        }
        // POST: WitnessCard/Create
        [HttpPost]
        public ActionResult Suggest(SuggestModel suggest)
        {
            //if (!ModelState.IsValid || Image == null || Image.ContentLength == 0)
            //{
            //    RedirectToAction("Create");

            //}
            // User.Identity.GetUserId();
            int UserId = UserManager.FindById(User.Identity.GetUserId()).UserInfo.Id;


            int Id = suggest.IdWC;
            string Name = suggest.name;
            SuggestionWC suggestionWC = new SuggestionWC()
            {
                SuggestingUserId = UserId,
                SuggestedUserId = 2,
                SuggestedCardId = Id,
                DateSuggestion = DateTime.Now
            };

            witnessCardServices.Suggest(suggestionWC);
            witnessCardServices.Commit();

            return RedirectToAction("Index");

        }


        public ActionResult ListOfWCSuggested()
        {
            int UserId = UserManager.FindById(User.Identity.GetUserId()).UserInfo.Id;
            var witnessCard = witnessCardServices.ListOfWCSuggested(UserId);
            List<WitnessCardModel> WCM = new List<WitnessCardModel>();
            foreach (var item in witnessCard)
            {
                WCM.Add(
                    new WitnessCardModel
                    {
                        name = item.name,
                        description = item.description,
                        picture = item.picture,
                        //CategoryId = item.CategoryId
                        Id = item.Id
                    });




            }
            return View(WCM);
        }

        [HttpPost]
        public ActionResult Index(String searchString)
        {

            var witnessCard = witnessCardServices.GetAll();
            List<WitnessCardModel> WCM = new List<WitnessCardModel>();
            foreach (var item in witnessCard)
            {
                WCM.Add(
                    new WitnessCardModel
                    {
                        name = item.name,
                        description = item.description,
                        picture = item.picture,
                        //CategoryId = item.CategoryId,
                        Id = item.Id
                    });
            }

            if (!String.IsNullOrEmpty(searchString)) { WCM = WCM.Where(m => m.name.Contains(searchString)).ToList(); }
            return View(WCM);
        }


        // GET: WitnessCard/Details/5
        public ActionResult Details(int id)
        {
            WitnessCard wc = witnessCardServices.GetById(id);

            WitnessCardModel WCM = new WitnessCardModel
            {
                name = wc.name,
                description = wc.description,
                picture = wc.picture,
                //CategoryId = item.CategoryId
                Id = wc.Id,
                Localisation = wc.Address,


            };

            WCM.Category = categoryServices.GetAll().ToSelectListItems();

            return View(WCM);

        }

        // GET: WitnessCard/Create
        public ActionResult Create()
        {
            var WCModel = new WitnessCardModel();
            WCModel.Category = categoryServices.GetAll().ToSelectListItems();
            return View(WCModel);

        }
        
        // POST: WitnessCard/Create
        [HttpPost]
        public ActionResult Create(WitnessCardModel wc, HttpPostedFileBase Image)
        {
            //if (!ModelState.IsValid || Image == null || Image.ContentLength == 0)
            //{
            //    RedirectToAction("Create");

            //}

            wc.picture = Image.FileName;
            WitnessCard witnessCard = new WitnessCard
            {

                name = wc.name,
                description = wc.description,
                picture = wc.picture,
                creatorId = UserManager.FindById(User.Identity.GetUserId()).UserInfo.Id,
                note = 0,
                CategoryId = wc.CategoryId,
                Address = wc.Localisation
            };

            witnessCardServices.Add(witnessCard);
            witnessCardServices.Commit();
            var path = Path.Combine(Server.MapPath("~/Content/_Uploads/Images/"), Image.FileName);
            Image.SaveAs(path);
            return RedirectToAction("MyWitnessCards");

        }

        // GET: WitnessCard/Edit/5
        public ActionResult Edit(int id)
        {
            WitnessCard wc = witnessCardServices.GetById(id);

            WitnessCardModel WCM = new WitnessCardModel
            {
                name = wc.name,
                description = wc.description,
                picture = wc.picture,
                //CategoryId = item.CategoryId
                Id = wc.Id,
                Localisation = wc.Address,


            };

            WCM.Category = categoryServices.GetAll().ToSelectListItems();

            return View(WCM);

        }

        // POST: WitnessCard/Edit/5
        [HttpPost]
        public ActionResult Edit(int id, WitnessCardModel wc, HttpPostedFileBase Image)
        {
            WitnessCard witnessCard = witnessCardServices.GetById(id);
            wc.picture = Image.FileName;

            witnessCard.name = wc.name;
            witnessCard.description = wc.description;
            witnessCard.picture = wc.picture;
            witnessCard.note = 0;
            witnessCard.CategoryId = wc.CategoryId;
            witnessCard.Address = wc.Localisation;
            
            witnessCardServices.Update(witnessCard);
            witnessCardServices.Commit();
            var path = Path.Combine(Server.MapPath("~/Content/Upload/"), Image.FileName);
            Image.SaveAs(path);
            return RedirectToAction("Index");

        }

        // GET: WitnessCard/Delete/5
        public ActionResult Delete(int id)
        {
            WitnessCard wc = witnessCardServices.GetById(id);

            WitnessCardModel WCM = new WitnessCardModel
            {
                name = wc.name,
                description = wc.description,
                picture = wc.picture,
                //CategoryId = item.CategoryId
                Id = wc.Id
            };
            WCM.Category = categoryServices.GetAll().ToSelectListItems();
            return View(WCM);

        }

        // POST: WitnessCard/Delete/5
        [HttpPost]
        public ActionResult Delete(int id, FormCollection collection)
        {
            WitnessCard witnessCard = witnessCardServices.GetById(id);
            witnessCardServices.Delete(witnessCard);
            witnessCardServices.Commit();
            return RedirectToAction("Index");
        }
        public ActionResult Afficher(int CategoryId)
        {
            List<WitnessCard> wc = witnessCardServices.GetAllByCategoryId(CategoryId).ToList();
            List<WitnessCardModel> WCM = new List<Models.WitnessCardModel>();
            foreach (var item in wc)
            {
                WCM.Add(
                    new WitnessCardModel
                    {
                        name = item.name,
                        description = item.description,
                        picture = item.picture,
                        //CategoryId = item.CategoryId,
                        Id = item.Id
                    });
            }
            return View(WCM);
        }
        public ActionResult Publish(int id)
        {
            witnessCardServices.publishWC(id);
            return RedirectToAction("MyWitnessCards");
        }
        public ActionResult Lister()
        {
            var Categories = categoryServices.GetAll();
            List<CategoryModel> WCM = new List<CategoryModel>();
            foreach (var item in Categories)
            {
                WCM.Add(
                    new CategoryModel
                    {
                        name = item.name,
                        IDCat = item.Id

                    });
            }

            return View(WCM);

        }

        // GET: TopWitnessCard
        public ActionResult TopTen(int id)
        {

            var witnessCard = witnessCardServices.GetBestTenWitnessCardsByCategoryId(id);
            List<WitnessCard> witnesscard = witnessCard.ToList();
            List<IndexWCViewModel> WCM = new List<IndexWCViewModel>();
            foreach (WitnessCard item in witnesscard)
            {

                WCM.Add(
                    new IndexWCViewModel
                    {
                        name = item.name,
                        description = item.description,
                        Id = item.Id,
                        note = item.note,
                        picture = item.picture

                    });
            }
            return View(WCM);


        }

        // GET: WitnessCard/Edit/5
        public ActionResult NoteWitnessCard(int id)
        {

            WitnessCard witnesscard = witnessCardServices.GetById(id);
            WitnessCardViewModel witnesscardview = new WitnessCardViewModel();

            List<string> criterias = witnessCardServices.GetBestThreeCriteriasByCategory(witnesscard.CategoryId).ToList();
            witnesscardview.crateria1 = criterias[0];
            witnesscardview.crateria2 = criterias[1];
            witnesscardview.crateria3 = criterias[2];

            return View(witnesscardview);
        }

        // POST: WitnessCard/Edit/5
        [HttpPost]
        public ActionResult UpdateNoteWitnessCard(int id, FormCollection collection, WitnessCardViewModel witnesscardview)
        {

            int note1 = witnesscardview.note1;
            int note2 = witnesscardview.note2;
            int note3 = witnesscardview.note3;
            WitnessCard witnesscard = new WitnessCard();
            witnesscard = witnessCardServices.GetById(id);
            witnessCardServices.AddWitnessCardNote(note1, note2, note3, witnesscard);

            return RedirectToAction("Index");

        }

        // GET: SubscribeToWitnessCard
        public ActionResult ListWitnessCardSubscription()
        {
            var wc = witnessCardServices.GetAll();
            List<WitnessCardModel> wcm = new List<WitnessCardModel>();

            User loggedUser = UserManager.FindById(User.Identity.GetUserId()).UserInfo;

            foreach (var w in wc)
            {
                wcm.Add(new WitnessCardModel
                {
                    Id = w.Id,
                    name = w.name,
                    description = w.description,
                    note = w.note,
                    picture = w.picture,
                    isSubscribed = subscribeService.isSubscribed(w, loggedUser)

                });

            }
            return View(wcm);
        }

        // GET: SubscribeToWitnessCard/Subscribe
        public ActionResult Subscribe(int id)
        {

            try
            {
                subscribeService.subscribeToWitnessCard(id, 1);
                subscribeService.Commit();
                subscribeService.sendConfirmationByMail(1, "You have been subscribed to", id);
                return RedirectToAction("ListWitnessCardSubscription");
            }
            catch
            {
                return RedirectToAction("SubscribeFailed");
            }
        }
        public ActionResult unSubscribe(int id)
        {

            try
            {
                subscribeService.unsubscribeToWitnessCard(id, 1);
                subscribeService.Commit();
                subscribeService.sendConfirmationByMail(1, "You have been unsubscribed from", id);
                return RedirectToAction("ListWitnessCardSubscription");
            }
            catch
            {
                return RedirectToAction("SubscribeFailed");
            }
        }
        public ActionResult SubscribeOK()
        {
            return View();
        }
        public ActionResult SubscribeFailed()
        {
            return View();
        }

        // GET: Suggestion
        public ActionResult RandomSuggestedCards()
        {
            User loggedUser = UserManager.FindById(User.Identity.GetUserId()).UserInfo;

            List<WitnessCard> SuggestedWitnessCard = suggestionService.suggestWitnessCards(loggedUser.Id);
            List<WitnessCardModel> wcm = new List<WitnessCardModel>();
            foreach (var w in SuggestedWitnessCard)
            {
                wcm.Add(new WitnessCardModel
                {
                    Id = w.Id,
                    name = w.name,
                    description = w.description,
                    note = w.note,
                    picture = w.picture,
                });
            }
            return View(wcm);
        }

    }
}
