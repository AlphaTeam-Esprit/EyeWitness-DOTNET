﻿using EyeWitness.Domain.Entities;
using EyeWitness.GUI.Identity.Models;
using EyeWitness.Services.Repositories;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Net;
using System.Net.Mail;
using System.Web;
using System.Web.Mvc;

namespace EyeWitness.GUI.Identity.Controllers
{
    public class CategoryController : Controller
    {
        CategoryServices categoryServices;

        public CategoryController()
        {
            categoryServices = new CategoryServices();
        }
        // GET: Category/Create
        public ActionResult Create()
        {

            var categoryviewmodel = new CategoryViewModel();

            return View(categoryviewmodel);
        }

        // POST: Category/Create
        [HttpPost]
        public ActionResult Create(FormCollection collection, CategoryViewModel categorymodelview)
        {
            try
            {
                Category category = new Category
                {
                    name = categorymodelview.name,
                    Status = "REQUESTED"

                };
                categoryServices.Add(category);
                categoryServices.Commit();
                SmtpClient smtp = new SmtpClient();
                MailMessage msg = new MailMessage();
                smtp.Credentials = new NetworkCredential("ahmedmoalla@gmail.com", "ahmedmoalla");
                smtp.Host = "smtp.gmail.com";
                smtp.Port = 587;
                smtp.EnableSsl = true;
                smtp.DeliveryMethod = SmtpDeliveryMethod.Network;
                smtp.Timeout = 20000;
                msg.From = new MailAddress("ahmedmoalla@gmail.com");
                msg.To.Add("benmarzoukkhaled@gmail.com");
                msg.Body = "Un utilisateur suggére une categorie" + category.name;
                smtp.Send(msg);
                return RedirectToAction("Create");

            }
            catch
            {
                return View();
            }
        }
    }
}
