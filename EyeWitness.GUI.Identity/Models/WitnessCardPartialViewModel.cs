﻿using EyeWitness.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class WitnessCardPartialViewModel
    {

        public WitnessCardPartialViewModel(WitnessCard card)
        {
            Category = card.category.name;
            Note = (int)card.note;
            Name = card.name;
            Description = card.description;
            NbEvaluations = card.evaluations == null ? 0 : card.evaluations.Count;
            NbSubscribers = card.subscriptionsWC == null ? 0 : card.subscriptionsWC.Count;
            ImageUrl = card.picture;
            Longitude = card.Longitude;
            Latitude = card.Latitude;
        }

        public string Category { get; set; }
        public int Note { get; set; }
        public string Name { get; set; }
        public string Description { get; set; }
        public int NbEvaluations { get; set; }
        public int NbSubscribers { get; set; }
        public string ImageUrl { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
    }
}