﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EyeWitness.GUI.Identity.Models
{
    public class WitnessCardModel
    {
        public int Id { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string status { get; set; }
        [DataType(DataType.ImageUrl), Display(Name = "Picture")]
        public string picture { get; set; }
        [Display(Name = "Category")]
        public int CategoryId { get; set; }
        public IEnumerable<SelectListItem> Category { get; set; }
        public String latitude { get; set; }
        public String Longitude { get; set; }
        [Display(Name = "Adresse")]
        public String Localisation { get; set; }
        public float note { get; set; }
        public string isSubscribed { get; set; }
    }
}