﻿using System.Data.Entity;
using System.Security.Claims;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.EntityFramework;
using EyeWitness.Domain.Entities;
using System.ComponentModel.DataAnnotations.Schema;

namespace EyeWitness.GUI.Identity.Models
{
    // You can add profile data for the user by adding more properties to your ApplicationUser class, please visit http://go.microsoft.com/fwlink/?LinkID=317594 to learn more.
    public class ApplicationUser : IdentityUser
    {
        public async Task<ClaimsIdentity> GenerateUserIdentityAsync(UserManager<ApplicationUser> manager)
        {
            // Note the authenticationType must match the one defined in CookieAuthenticationOptions.AuthenticationType
            var userIdentity = await manager.CreateIdentityAsync(this, DefaultAuthenticationTypes.ApplicationCookie);
            // Add custom user claims here
            return userIdentity;
        }

        [ForeignKey("UserInfo")]
        public int UserId;
        public virtual User UserInfo { get; set; }
    }

    
    public class ApplicationDbContext : IdentityDbContext<ApplicationUser>
    {
        public ApplicationDbContext()
            : base("WitnessConnectionString", throwIfV1Schema: false)
        {
        }

        protected override void OnModelCreating(DbModelBuilder modelBuilder)
        {
            base.OnModelCreating(modelBuilder);
            modelBuilder.Entity<ChoosenCriterias>().HasKey(cc => new { cc.CategoryId, cc.CriteriasId });
            modelBuilder.Entity<SubscriptionsWC>().HasKey(sub => new { sub.WitnessCardId, sub.UserId });
            modelBuilder.Entity<Evaluation>().HasKey(ev => new { ev.WitnessCardId, ev.UserId, ev.evaluationDate });
            modelBuilder.Entity<User>().HasMany(w => w.witnessCardManaged).WithOptional(a => a.agent).WillCascadeOnDelete(false);
            modelBuilder.Entity<User>().HasMany(w => w.witnessCardCreated).WithRequired(a => a.creator).WillCascadeOnDelete(false);
            modelBuilder.Entity<User>().HasMany(f => f.followers).WithMany().Map(m =>
            {
                m.MapLeftKey("UserId");
                m.MapRightKey("FollowerId");
                m.ToTable("Followers");
            }
            );
            modelBuilder.Entity<SuggestionWC>().HasKey(c => new { c.SuggestingUserId, c.SuggestedUserId, c.SuggestedCardId });
            modelBuilder.Entity<User>().HasMany(w => w.SuggestingSuggestions).WithRequired(w => w.SuggestingUser).WillCascadeOnDelete(false);
            modelBuilder.Entity<User>().HasMany(w => w.SuggestedSuggestions).WithRequired(w => w.SuggestedUser).WillCascadeOnDelete(false);
        }

        public static ApplicationDbContext Create()
        {
            return new ApplicationDbContext();
        }
    }
}