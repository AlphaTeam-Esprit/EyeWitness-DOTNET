﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class IndexWCViewModel
    {
        
        public int Id { get; set; }
        public string status { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        [DataType(DataType.ImageUrl), Display(Name = "picture")]
        public string picture { get; set; }
        public float note { get; set; }
    }
}