﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class ChatViewModel
    {
        public int LoggedUserId { get; set; }
        public ICollection<ChatUser> Followers { get; set; }
    }

    public class ChatUser
    {
        public string Avatar { get; set; }
        public string FullName { get; set; }
        public int Id { get; set; }
    }
}