﻿using EyeWitness.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class CriteriaVoteViewModel
    {

        public ICollection<CriteriaViewModel> CriteriaModels { get; set; }
        public int TotalVotes { get; set; }
        public Category Category { get; set; }

    }
}