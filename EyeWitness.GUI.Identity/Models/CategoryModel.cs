﻿using EyeWitness.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class CategoryModel
    {
        public string name { get; set; }
        public int IDCat { get; set; }
        List<WitnessCard> witnessCards { get; set; }
    }
}