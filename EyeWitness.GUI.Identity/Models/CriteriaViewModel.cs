﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class CriteriaViewModel
    {
        public int Id { get; set; }
        public string Name { get; set; }
        public int CurrentVotes { get; set; }
        public float PercentTotalVotes { get; set; }
        public int AverageRating { get; set; }
    }
}