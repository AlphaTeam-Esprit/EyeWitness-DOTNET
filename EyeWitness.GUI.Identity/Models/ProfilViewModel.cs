﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class ProfilViewModel
    {
        public int Id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }

        public DateTime birthDate { get; set; }

        public string password { get; set; }

        public string email { get; set; }

        public string address { get; set; }
        [DataType(DataType.ImageUrl), Display(Name = "avatar")]
        public string avatar { get; set; }

        public string phoneNumber { get; set; }
    }
}