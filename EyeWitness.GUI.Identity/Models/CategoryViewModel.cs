﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class CategoryViewModel
    {
        public int Id { get; set; }
        public string name { get; set; }
        public String Status { get; set; }

    }
}