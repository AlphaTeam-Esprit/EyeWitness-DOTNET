﻿using EyeWitness.Domain.Entities;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class WitnessCardViewModel
    {

        public string crateria1 { get; set; }
        public string crateria2 { get; set; }
        public string crateria3 { get; set; }
        [Range(1, 5,ErrorMessage = "Note must be between 1 and 5")]
        public int note1 { get; set; }
        [Range(1, 5,ErrorMessage = "Note must be between 1 and 5")]
        public int note2 { get; set; }
        [Range(1, 5,ErrorMessage = "Note must be between 1 and 5")]
        public int note3 { get; set; }
    }
}