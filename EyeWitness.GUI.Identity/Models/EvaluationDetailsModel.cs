﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class EvaluationDetailsModel
    {
        
        public string WCPicture { get; set; }
        public string WCDescription { get; set; }
        public string comment { get; set; }
        public string img { get; set; }
        public string video { get; set; }
        public int rating { get; set; }

    }
}