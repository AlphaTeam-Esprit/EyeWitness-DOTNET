﻿using EyeWitness.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class EvaluationViewModel
    {
        public DateTime evaluationDate { get; set; }
        public string comment { get; set; }
        public string video { get; set; }
        public int rating { get; set; }
        public string picture { get; set; }

        public int UserId { get; set; }
        public virtual User user { get; set; }
        public IEnumerable<Evaluation> evaluations { get; set; }
    }
}