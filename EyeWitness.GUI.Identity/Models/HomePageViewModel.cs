﻿using EyeWitness.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class HomePageViewModel
    {
        public IEnumerable<WitnessCardPartialViewModel> PopularWitnessCards { get; set; }
        public string Name { get; set; }
    }
}