﻿using EyeWitness.Domain.Entities;
using EyeWitness.Domain.SearchCriterias;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EyeWitness.GUI.Identity.Models
{
    public class SearchViewModel
    {
        public IEnumerable<SelectListItem> Categories { get; set; }
        public String Name { get; set; }
        public String OrderBy { get; set; }
        public IEnumerable<SelectListItem> OrderByItems { get; set; }
        public IEnumerable<SelectListItem> MeansOfPayment { get; set; }
        public IEnumerable<SelectListItem> OfferedServices { get; set; }
        public IEnumerable<SelectListItem> Ambiences { get; set; }
        public String Longitude { get; set; }
        public String Latitude { get; set; }
        public String Address { get; set; }
        public bool NearMe { get; set; }
        public IEnumerable<WitnessCardPartialViewModel> SearchResults { get; set; }

        public int CurrentPage { get; set; }

        IEnumerable<WitnessCard> Cards { get; set; }

        public IEnumerable<WitnessCardPartialViewModel> GetSearchResults(int nbPerPage = 6)
        {
            return SearchResults.Skip((CurrentPage - 1) * nbPerPage).Take(nbPerPage);
        }
    }
}