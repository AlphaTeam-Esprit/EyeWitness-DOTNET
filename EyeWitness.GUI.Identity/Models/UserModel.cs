﻿using EyeWitness.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;

namespace EyeWitness.GUI.Identity.Models
{
    public class UserModel
    {
        public int Id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }

   
        public string avatar { get; set; }

    
    }
}