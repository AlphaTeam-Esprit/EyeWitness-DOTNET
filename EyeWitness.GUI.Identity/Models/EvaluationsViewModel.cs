﻿using EyeWitness.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EyeWitness.GUI.Identity.Models
{
    public class EvaluationsViewModel
    {
        public EvaluationsViewModel()
        {
            Ratings = new List<SelectListItem>();
            Ratings.Add(new SelectListItem() { Text = "1", Value = "1" });
            Ratings.Add(new SelectListItem() { Text = "2", Value = "2" });
            Ratings.Add(new SelectListItem() { Text = "3", Value = "3" });
            Ratings.Add(new SelectListItem() { Text = "4", Value = "4" });
            Ratings.Add(new SelectListItem() { Text = "5", Value = "5" });
        }
        public EvaluationsViewModel(Evaluation e)
        {
            UserImageUrl = e.user.avatar;
            UserFirstName = e.user.firstName;
            UserLastName = e.user.lastName;
            DateOfEvaluation = e.evaluationDate;
            VideoUrl = e.video;
            Rating = e.rating;
            Description = e.comment;
            ImagesUrls = e.GetPicturesUrls();
        }
        public int WitnessCardId { get; set; }
        public WitnessCard WitnessCard { get; set; }
        public string UserImageUrl { get; set; }
        public string UserFirstName { get; set; }
        public string UserLastName { get; set; }
        public DateTime DateOfEvaluation { get; set; }
        public string VideoUrl { get; set; }
        public int Rating { get; set; }
        public List<SelectListItem> Ratings { get; set; }
        public string Description { get; set; }
        public string[] ImagesUrls { get; set; }
    }
}