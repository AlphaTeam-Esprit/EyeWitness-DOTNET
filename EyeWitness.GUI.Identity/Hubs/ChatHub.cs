﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using Microsoft.AspNet.SignalR;
using System.Threading.Tasks;
using Microsoft.AspNet.Identity;
using Microsoft.AspNet.Identity.Owin;
using EyeWitness.Domain.Entities;
using EyeWitness.GUI.Identity.Models;

namespace EyeWitness.GUI.Identity.Hubs
{
    public class Rooms
    {
        private static Rooms Instance;
        public List<Room> RoomsList;
        public ApplicationDbContext ctx;

        private Rooms()
        {
            RoomsList = new List<Room>();
            ctx = new ApplicationDbContext();
        }

        public static Rooms GetInstance()
        {
            if (Instance == null) Instance = new Rooms();
            return Instance;
        }

    }

    public class Room {
        public string Id { get; set; }
        public string Id2 { get; set; }
        public int IdUser1 { get; set; }
        public int IdUser2 { get; set; }

        public string GetId()
        {
            return Id;
        }
    }

    public class ChatHub : Hub
    {

        public void SendMessageTo(int senderId, string idRoom, string message)
        {
            string[] ids = idRoom.Split('_');
            Room roomFound = FindRoomOf(int.Parse(ids[0]), int.Parse(ids[1]));
            LogToRoom(roomFound.GetId(), message);

            ApplicationUserManager UserManager = Context.Request.GetHttpContext().GetOwinContext().GetUserManager<ApplicationUserManager>();
            string loggedApplicationUserId = Context.User.Identity.GetUserId();
            ApplicationUser loggedApplicationUser = Rooms.GetInstance().ctx.Users.Find(loggedApplicationUserId);
            User loggedUserInfo = loggedApplicationUser.UserInfo;

            Clients.Group(roomFound.GetId()).addMessage(loggedUserInfo.Id, roomFound.GetId(), message);
        }

        public override Task OnConnected()
        {
            ApplicationUserManager UserManager = Context.Request.GetHttpContext().GetOwinContext().GetUserManager<ApplicationUserManager>();
            User loggedUser = UserManager.FindById(Context.User.Identity.GetUserId()).UserInfo;
            LogToUser("Number of rooms : " + Rooms.GetInstance().RoomsList.Count);
            ICollection<User> followers = loggedUser.followers;

            foreach(User u in followers)
            {
                // Check if room already exists
                Room existingRoom = FindRoomOf(loggedUser.Id, u.Id);
                LogToUser("Trying to join Room for user Id : " + loggedUser.Id);
                // If it doesn't
                if (existingRoom == null)
                {
                    Room newRoom = CreateRoomFor(loggedUser.Id, u.Id);
                    LogToUser("Room doesn't exist. Creating a new Room with Id : " + newRoom.GetId());
                    // Create a new room
                    Groups.Add(Context.ConnectionId, newRoom.GetId());
                }
                else
                {
                    // Use existing room
                    LogToUser("Found an existing Room with Id : " + existingRoom.GetId());
                    Groups.Add(Context.ConnectionId, existingRoom.GetId());
                }
                    
            }

            return base.OnConnected();
        }

        public Room CreateRoomFor(int id1, int id2)
        {
            Room room = new Room()
            {
                Id = id1 + "_" + id2,
                Id2 = id2 + "_" + id1,
                IdUser1 = id1,
                IdUser2 = id2
            };

            Rooms.GetInstance().RoomsList.Add(room);

            return room;
        }

        public Room FindRoomOf(int id1, int id2)
        {
            IEnumerable<Room> room = Rooms.GetInstance().RoomsList.Where(r => (r.IdUser1.Equals(id1) && r.IdUser2.Equals(id2)) || (r.IdUser1.Equals(id2) && r.IdUser2.Equals(id1)));

            if (room.Count().Equals(0)) return null;
            else return room.First();
        }

        public void LogToUser(string msg)
        {
            Clients.Caller.log(msg);
        }

        public void LogToRoom(string roomId, string msg)
        {
            Clients.Group(roomId).log(msg);
        }

    }
}