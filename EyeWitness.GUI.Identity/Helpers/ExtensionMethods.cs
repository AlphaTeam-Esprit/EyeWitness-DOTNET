﻿using EyeWitness.Domain.Entities;
using EyeWitness.GUI.Identity.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Web;
using System.Web.Mvc;

namespace EyeWitness.GUI.Identity.Helpers
{
    public static class ExtensionMethods
    {
        public static IEnumerable<WitnessCardPartialViewModel> ToViewModelEnumerable(this IEnumerable<WitnessCard> e)
        {
            List<WitnessCardPartialViewModel> res = new List<WitnessCardPartialViewModel>();

            foreach (WitnessCard c in e) res.Add(new WitnessCardPartialViewModel(c));

            return res;
        }

        public static IEnumerable<EvaluationsViewModel> ToViewModelEnumerable(this IEnumerable<Evaluation> evals)
        {
            List<EvaluationsViewModel> res = new List<EvaluationsViewModel>();

            foreach (Evaluation e in evals) res.Add(new EvaluationsViewModel(e));

            return res;
        }

        public static IEnumerable<SelectListItem> ToSelectListItems(
           this IEnumerable<Category> Category)
        {
            return
                Category.OrderBy(cat => cat.name)
                      .Select(cat =>
                          new SelectListItem
                          {

                              Text = cat.name,
                              Value = cat.Id.ToString()
                          });
        }
    }
}