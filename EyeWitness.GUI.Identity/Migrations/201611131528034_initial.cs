namespace EyeWitness.GUI.Identity.Migrations
{
    using System;
    using System.Data.Entity.Migrations;
    
    public partial class initial : DbMigration
    {
        public override void Up()
        {
            CreateTable(
                "dbo.AspNetRoles",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Name = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => t.Id)
                .Index(t => t.Name, unique: true, name: "RoleNameIndex");
            
            CreateTable(
                "dbo.AspNetUserRoles",
                c => new
                    {
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        RoleId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => new { t.UserId, t.RoleId })
                .ForeignKey("dbo.AspNetRoles", t => t.RoleId, cascadeDelete: true)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId)
                .Index(t => t.RoleId);
            
            CreateTable(
                "dbo.AspNetUsers",
                c => new
                    {
                        Id = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        Email = c.String(maxLength: 256, storeType: "nvarchar"),
                        EmailConfirmed = c.Boolean(nullable: false),
                        PasswordHash = c.String(unicode: false),
                        SecurityStamp = c.String(unicode: false),
                        PhoneNumber = c.String(unicode: false),
                        PhoneNumberConfirmed = c.Boolean(nullable: false),
                        TwoFactorEnabled = c.Boolean(nullable: false),
                        LockoutEndDateUtc = c.DateTime(precision: 0),
                        LockoutEnabled = c.Boolean(nullable: false),
                        AccessFailedCount = c.Int(nullable: false),
                        UserName = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        UserInfo_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Users", t => t.UserInfo_Id)
                .Index(t => t.UserName, unique: true, name: "UserNameIndex")
                .Index(t => t.UserInfo_Id);
            
            CreateTable(
                "dbo.AspNetUserClaims",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        ClaimType = c.String(unicode: false),
                        ClaimValue = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.AspNetUserLogins",
                c => new
                    {
                        LoginProvider = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        ProviderKey = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                        UserId = c.String(nullable: false, maxLength: 128, storeType: "nvarchar"),
                    })
                .PrimaryKey(t => new { t.LoginProvider, t.ProviderKey, t.UserId })
                .ForeignKey("dbo.AspNetUsers", t => t.UserId, cascadeDelete: true)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.Users",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        firstName = c.String(unicode: false),
                        lastName = c.String(unicode: false),
                        birthDate = c.DateTime(nullable: false, precision: 0),
                        password = c.String(unicode: false),
                        email = c.String(unicode: false),
                        address = c.String(unicode: false),
                        avatar = c.String(unicode: false),
                        phoneNumber = c.String(unicode: false),
                        noteXP = c.Single(nullable: false),
                        role = c.String(unicode: false),
                        status = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Evaluations",
                c => new
                    {
                        WitnessCardId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        evaluationDate = c.DateTime(nullable: false, precision: 0),
                        comment = c.String(unicode: false),
                        video = c.String(unicode: false),
                        rating = c.Int(nullable: false),
                        pictures = c.String(unicode: false),
                    })
                .PrimaryKey(t => new { t.WitnessCardId, t.UserId, t.evaluationDate })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.WitnessCards", t => t.WitnessCardId, cascadeDelete: true)
                .Index(t => t.WitnessCardId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.WitnessCards",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        status = c.String(unicode: false),
                        name = c.String(unicode: false),
                        description = c.String(unicode: false),
                        picture = c.String(unicode: false),
                        note = c.Single(nullable: false),
                        creatorId = c.Int(nullable: false),
                        agentId = c.Int(),
                        CategoryId = c.Int(nullable: false),
                        Ambience = c.String(unicode: false),
                        OfferedServices = c.String(unicode: false),
                        MeansOfPayment = c.String(unicode: false),
                        Longitude = c.Single(nullable: false),
                        Latitude = c.Single(nullable: false),
                        Address = c.String(unicode: false),
                        nbuser = c.Int(nullable: false),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.creatorId)
                .ForeignKey("dbo.Users", t => t.agentId)
                .Index(t => t.creatorId)
                .Index(t => t.agentId)
                .Index(t => t.CategoryId);
            
            CreateTable(
                "dbo.Categories",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(unicode: false),
                        Status = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.ChoosenCriterias",
                c => new
                    {
                        CategoryId = c.Int(nullable: false),
                        CriteriasId = c.Int(nullable: false),
                        vote = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.CategoryId, t.CriteriasId })
                .ForeignKey("dbo.Categories", t => t.CategoryId, cascadeDelete: true)
                .ForeignKey("dbo.Criterias", t => t.CriteriasId, cascadeDelete: true)
                .Index(t => t.CategoryId)
                .Index(t => t.CriteriasId);
            
            CreateTable(
                "dbo.Criterias",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(unicode: false),
                    })
                .PrimaryKey(t => t.Id);
            
            CreateTable(
                "dbo.Competitions",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        theme = c.String(unicode: false),
                        descripiton = c.String(unicode: false),
                        dateFrom = c.DateTime(nullable: false, precision: 0),
                        dateTo = c.DateTime(nullable: false, precision: 0),
                        WitnessCard_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WitnessCards", t => t.WitnessCard_Id)
                .Index(t => t.WitnessCard_Id);
            
            CreateTable(
                "dbo.Events",
                c => new
                    {
                        Id = c.Int(nullable: false, identity: true),
                        name = c.String(unicode: false),
                        description = c.String(unicode: false),
                        dateFrom = c.DateTime(nullable: false, precision: 0),
                        dateTo = c.DateTime(nullable: false, precision: 0),
                        picture = c.String(unicode: false),
                        witnessCard_Id = c.Int(),
                    })
                .PrimaryKey(t => t.Id)
                .ForeignKey("dbo.WitnessCards", t => t.witnessCard_Id)
                .Index(t => t.witnessCard_Id);
            
            CreateTable(
                "dbo.SubscriptionsWCs",
                c => new
                    {
                        WitnessCardId = c.Int(nullable: false),
                        UserId = c.Int(nullable: false),
                        subscriptionDate = c.DateTime(nullable: false, precision: 0),
                        Type = c.String(unicode: false),
                    })
                .PrimaryKey(t => new { t.WitnessCardId, t.UserId })
                .ForeignKey("dbo.Users", t => t.UserId, cascadeDelete: true)
                .ForeignKey("dbo.WitnessCards", t => t.WitnessCardId, cascadeDelete: true)
                .Index(t => t.WitnessCardId)
                .Index(t => t.UserId);
            
            CreateTable(
                "dbo.SuggestionWCs",
                c => new
                    {
                        SuggestingUserId = c.Int(nullable: false),
                        SuggestedUserId = c.Int(nullable: false),
                        SuggestedCardId = c.Int(nullable: false),
                        DateSuggestion = c.DateTime(nullable: false, precision: 0),
                    })
                .PrimaryKey(t => new { t.SuggestingUserId, t.SuggestedUserId, t.SuggestedCardId })
                .ForeignKey("dbo.WitnessCards", t => t.SuggestedCardId, cascadeDelete: true)
                .ForeignKey("dbo.Users", t => t.SuggestedUserId)
                .ForeignKey("dbo.Users", t => t.SuggestingUserId)
                .Index(t => t.SuggestingUserId)
                .Index(t => t.SuggestedUserId)
                .Index(t => t.SuggestedCardId);
            
            CreateTable(
                "dbo.Followers",
                c => new
                    {
                        UserId = c.Int(nullable: false),
                        FollowerId = c.Int(nullable: false),
                    })
                .PrimaryKey(t => new { t.UserId, t.FollowerId })
                .ForeignKey("dbo.Users", t => t.UserId)
                .ForeignKey("dbo.Users", t => t.FollowerId)
                .Index(t => t.UserId)
                .Index(t => t.FollowerId);
            
        }
        
        public override void Down()
        {
            DropForeignKey("dbo.AspNetUsers", "UserInfo_Id", "dbo.Users");
            DropForeignKey("dbo.WitnessCards", "agentId", "dbo.Users");
            DropForeignKey("dbo.WitnessCards", "creatorId", "dbo.Users");
            DropForeignKey("dbo.SuggestionWCs", "SuggestingUserId", "dbo.Users");
            DropForeignKey("dbo.SuggestionWCs", "SuggestedUserId", "dbo.Users");
            DropForeignKey("dbo.Followers", "FollowerId", "dbo.Users");
            DropForeignKey("dbo.Followers", "UserId", "dbo.Users");
            DropForeignKey("dbo.SuggestionWCs", "SuggestedCardId", "dbo.WitnessCards");
            DropForeignKey("dbo.SubscriptionsWCs", "WitnessCardId", "dbo.WitnessCards");
            DropForeignKey("dbo.SubscriptionsWCs", "UserId", "dbo.Users");
            DropForeignKey("dbo.Events", "witnessCard_Id", "dbo.WitnessCards");
            DropForeignKey("dbo.Evaluations", "WitnessCardId", "dbo.WitnessCards");
            DropForeignKey("dbo.Competitions", "WitnessCard_Id", "dbo.WitnessCards");
            DropForeignKey("dbo.WitnessCards", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.ChoosenCriterias", "CriteriasId", "dbo.Criterias");
            DropForeignKey("dbo.ChoosenCriterias", "CategoryId", "dbo.Categories");
            DropForeignKey("dbo.Evaluations", "UserId", "dbo.Users");
            DropForeignKey("dbo.AspNetUserRoles", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserLogins", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserClaims", "UserId", "dbo.AspNetUsers");
            DropForeignKey("dbo.AspNetUserRoles", "RoleId", "dbo.AspNetRoles");
            DropIndex("dbo.Followers", new[] { "FollowerId" });
            DropIndex("dbo.Followers", new[] { "UserId" });
            DropIndex("dbo.SuggestionWCs", new[] { "SuggestedCardId" });
            DropIndex("dbo.SuggestionWCs", new[] { "SuggestedUserId" });
            DropIndex("dbo.SuggestionWCs", new[] { "SuggestingUserId" });
            DropIndex("dbo.SubscriptionsWCs", new[] { "UserId" });
            DropIndex("dbo.SubscriptionsWCs", new[] { "WitnessCardId" });
            DropIndex("dbo.Events", new[] { "witnessCard_Id" });
            DropIndex("dbo.Competitions", new[] { "WitnessCard_Id" });
            DropIndex("dbo.ChoosenCriterias", new[] { "CriteriasId" });
            DropIndex("dbo.ChoosenCriterias", new[] { "CategoryId" });
            DropIndex("dbo.WitnessCards", new[] { "CategoryId" });
            DropIndex("dbo.WitnessCards", new[] { "agentId" });
            DropIndex("dbo.WitnessCards", new[] { "creatorId" });
            DropIndex("dbo.Evaluations", new[] { "UserId" });
            DropIndex("dbo.Evaluations", new[] { "WitnessCardId" });
            DropIndex("dbo.AspNetUserLogins", new[] { "UserId" });
            DropIndex("dbo.AspNetUserClaims", new[] { "UserId" });
            DropIndex("dbo.AspNetUsers", new[] { "UserInfo_Id" });
            DropIndex("dbo.AspNetUsers", "UserNameIndex");
            DropIndex("dbo.AspNetUserRoles", new[] { "RoleId" });
            DropIndex("dbo.AspNetUserRoles", new[] { "UserId" });
            DropIndex("dbo.AspNetRoles", "RoleNameIndex");
            DropTable("dbo.Followers");
            DropTable("dbo.SuggestionWCs");
            DropTable("dbo.SubscriptionsWCs");
            DropTable("dbo.Events");
            DropTable("dbo.Competitions");
            DropTable("dbo.Criterias");
            DropTable("dbo.ChoosenCriterias");
            DropTable("dbo.Categories");
            DropTable("dbo.WitnessCards");
            DropTable("dbo.Evaluations");
            DropTable("dbo.Users");
            DropTable("dbo.AspNetUserLogins");
            DropTable("dbo.AspNetUserClaims");
            DropTable("dbo.AspNetUsers");
            DropTable("dbo.AspNetUserRoles");
            DropTable("dbo.AspNetRoles");
        }
    }
}
