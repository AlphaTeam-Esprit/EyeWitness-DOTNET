﻿var map;
var yourLocationMarker;
var yourLocationListener;

function setHiddenInputOrderBy(value) {
    $('#orderByInput').val(value);
}

$(document).ready(function () {

    $('select').material_select();
    $('body > div.container').removeClass('container');
    $('.card > h5 > i').click(function () {
        $('.card-body').slideToggle();
    })

    $(window).resize(function () {
        $('#map').width($('body').width() - 50);
        $('#map').height($('body').height());
    })

    // Disable scrolling
    $('html, body').css({
        overflow: 'hidden',
        height: '100%'
    });

    $('#choose-location-btn').click(chooseLocation);

    function chooseLocation() {
        Materialize.toast("Choose a location on the map !", 1000);
        console.log('CLick !');
        $('#choose-location-btn').css('color', '#ccc');
        $('#choose-location-btn').unbind('click');
        yourLocationListener = google.maps.event.addListener(map, 'click', function (event) {

            if (yourLocationMarker != null) {
                // Delete it
                yourLocationMarker.setMap(null);
            }

            yourLocationMarker = new google.maps.Marker({
                position: event.latLng,
                map: map,
                title: 'Search location'
            });

            Materialize.toast('A location has been set', 1000);
            $('#lat').focus();
            $('#lat').val(event.latLng.lat);
            $('#lg').focus();
            $('#lg').val(event.latLng.lng);
            
            yourLocationListener.remove();

            // Enable Button
            $('#choose-location-btn').css('color', 'orange');
            $('#choose-location-btn').click(chooseLocation);
        });

    }
});

