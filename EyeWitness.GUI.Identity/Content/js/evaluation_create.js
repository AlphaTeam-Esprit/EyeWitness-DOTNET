﻿var nbImages = 0;
$(window).ready(function () {
    
    //video
    var URL = window.URL || window.webkitURL
    var displayMessage = function (message) {
        console.log(message);
    }
    var playSelectedFile = function (event) {
        var file = this.files[0]
        var type = file.type
        var videoNode = document.querySelector('video')
        var canPlay = videoNode.canPlayType(type)
        if (canPlay === '') canPlay = 'no'
        var message = 'Can play type "' + type + '": ' + canPlay
        var isError = canPlay === 'no'
        displayMessage(message)

        if (isError) {
            Materialize.toast('Cannot play this type of videos', 4000)
            return;
        }

        var fileURL = URL.createObjectURL(file)
        videoNode.src = fileURL

        $('.create-evaluation-video-placeholder > img').hide();
        $('.eval-video-container').show();
    }
    $('#Video').change(playSelectedFile);
    
    $('.eval-video-container > i.video-close-button').click(function () {
        $('.eval-video-container > video')[0].pause();
        $('.eval-video-container').hide();
        $('.create-evaluation-video-placeholder > img').show();      
    });

    //stars
    $('.stars > i').click(function () {

        var clickedIndex = $(this).index();
        for (i = 0; i <= clickedIndex; i++) {
            elt = $('.stars > i')[i];
            if ($(elt).hasClass('fa-star-o')) {
                $(elt).removeClass('fa-star-o');
                $(elt).addClass('fa-star');
            }
        }

        for (i = clickedIndex + 1; i < $('.stars > i').length; i++) {
            elt = $('.stars > i')[i];
            $(elt).removeClass('fa-star');
            $(elt).addClass('fa-star-o');
        }
        
        $('#Rating').val(clickedIndex + 1);
        $('.eval-rating > span').html(clickedIndex + 1);
    });


    $('.create-evaluation-video-placeholder > img').click(function () {
        $('.create-evaluation-video-placeholder > input[type=file]').click();
    });

    
    $('.add-image-file').click(function () {
        var elt = $('<div class="col s3 valign-wrapper file-elt" id="img_file_' + nbImages + '"><i class="fa fa-close tooltipped"  data-position="bottom" data-delay="50" data-tooltip="Remove this image"></i><input type="file" name="Images[' + nbImages + ']" id="files_' + nbImages + '" style="display: none" onchange="readUrl(this, \'img_file_' + nbImages + '\')"/></div>');
        $('.create-evaluation-image-placeholder').append($(elt));
        $('.tooltipped').tooltip({ delay: 50 });

        $('.file-elt > i').click(function () {
            var tooltipId = $(this).data('tooltip-id');
            $('#' + tooltipId).remove();
            $(this).parent().remove();
        });
        var fileInput = $('#files_' + nbImages);
        $(fileInput).click();
    });

});

function readUrl(input, imgId) {
    console.log("Changing !!");

    // Check for empty files_elt
    var fileElts = $('.file-elt');

    for (var i = 0; i < fileElts.length - 1; i++) { // not last elt because its the current and doesnt have imageAppendedSuccess class
        elt = $(fileElts)[i];
        //empty elt
        if (!$(elt).hasClass("imageAppendedSuccess")) {
            $(elt).remove();
        }
    }

    if (input.files && input.files[0]) {
        var reader = new FileReader();

        reader.onload = function (e) {
            $('#' + imgId).addClass('imageAppendedSuccess');
            $('#' + imgId).append($('<img src="' + e.target.result + '" />'));
            $('#' + imgId + ' > i').show();
            nbImages++;
        };

        reader.readAsDataURL(input.files[0]);
    }
}