﻿$(document).ready(function () {

    $('.chat-bubble').click(function () {
        $('.chat-bubble.selected').removeClass('selected');
        $(this).addClass('selected');

        var cardId = $(this).data('card-toggle');
        
        $('.chat-cards > div.card.selected').animate(
        {
            'right': '-500px'
        })
        
        setTimeout(function () {
            $('.chat-cards > div.card.selected').removeClass('selected');
            $('#' + cardId).addClass('selected');
            $('#' + cardId).animate({
                'right': '80px'
            });
        }, 300)
        

        // Find tag in chat-bubble element
        tagElt = $(this).find('.tag');

        // Remove tags
        tagElt.html('');
    
    });

});

function closeCard(Id) {
    $('.chat-cards > div.card.selected').animate(
    {
        'right': '-500px'
    });
    $('.chat-cards > div.card.selected').removeClass('selected');
    $('.chat-bubble.selected').removeClass('selected');
}