var menuIsOpen = false;
$(window).ready(function () {

    $('.tooltipped').tooltip({ delay: 50 });
    $('select').material_select();

    // Invisible navbar on home page
    if (window.location.pathname == "/" || window.location.pathname == "/Home/Index") {
        $('header > div > nav').css('background-color', 'transparent');
        $('header > div > nav').css('box-shadow', 'none');
    }
    $('#fast-search-results').hide();
    // Fast search
    $('#fast-search-input').focusin(function (e) {
        $('#fast-search-results').slideDown();
    });
    
    $('#fast-search-input').focusout(function (e) {
        $('#fast-search-results').slideUp();
    });
    
    //close menu when click outside of it
    $(document).click(function (event) {
        if (!$(event.target).closest('.side-menu-initial').length) {
            if (menuIsOpen) {
                closeMenu();
            }
        }
    })

    $('.menu-btn').click(function () {
        toggleMenu();
    });

    $('.menu-content > li.menu-item').click(function () {
        var href = $($(this).children()[0]).attr("href");
        window.location.href = href;
    })

});

function toggleMenu() {
    if (menuIsOpen) closeMenu();
    else openMenu();
}

function openMenu() {
    menuIsOpen = true;
    $('.side-menu-initial').animate({
        'width': '250px'
    });
    $('a.brand-logo > img, div.brand-text').animate({
        'left': '-100px'
    });
    $('.menu-content').animate({
        'left': '0px'
    });
}

function closeMenu() {
    menuIsOpen = false;
    $('.side-menu-initial').animate({
        'width': '50px'
    });
    $('a.brand-logo > img, div.brand-text').animate({
        'left': ''
    });
    $('.menu-content').animate({
        'left': '-500px'
    });
}