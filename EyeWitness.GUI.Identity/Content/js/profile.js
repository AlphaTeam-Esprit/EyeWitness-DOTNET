﻿$(document).ready(function () {
    $('body > div.container').css('margin-left', '50px');
    $('body > div.container').removeClass('container');

    $('ul.tabs').tabs();
    $('#personal-info-display').parent().height($('div[data-display=li1]').height() + 10);
    $('#personal-info-list > ul > li').click(function () {

        var liId = $(this).attr('id');

        $('#personal-info-list > ul > li').removeClass('selected');
        $(this).addClass('selected');

        $('#personal-info-display > div').each(function () {
            if ($(this).data('display') == liId) {
                height = $(this).height();
                if (height > 344)
                    $('#personal-info-display').parent().height(height + 10);
                else
                    $('#personal-info-display').parent().height(344);
                $(this).show();
            } else {
                $(this).hide();
            }
        });

    });

    $('#update-user-profile-button').click(function (e) {
        e.preventDefault();

        $('div[data-display=li2] > div.row > form').submit();
    })

    if (statusMessage != '') {
        Materialize.toast(statusMessage, 3000);
    }

    $('#personal-info-display > div:nth-child(5) > div > div').click(function () {
        var href = $(this).find('a').attr('href');
        window.location.pathname = href;
    })
});
