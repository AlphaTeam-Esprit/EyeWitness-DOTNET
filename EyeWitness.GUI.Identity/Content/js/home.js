﻿
$(document).ready(function () {
    $('.card-slick-slider').slick({
        swipe: true,
        touchMouve: false,
        infinite: false,
        slidesToShow: 3,
        slidesToScroll: 3
    });
});

jQuery(document).ready(function ($) {
    var sliderLeft = $.fn.fsvs({
        autoPlay: false,
        speed: 1000,
        bodyID: 'fsvs-body',
        selector: '> .slide',
        mouseSwipeDisance: 40,
        afterSlide: function (index) {
            console.log("afterSlide", index);
            if (index == 1) {
                $('#info-cards-content').css('display', '');
                $('.third-slide-title').css('display', '');
                $('.third-slide-title').addClass('slideInDown animated')
                $('#info-cards-content > div:nth-child(1)').addClass('bounceInLeft animated');
                $('#info-cards-content > div:nth-child(2)').addClass('bounceInUp animated');
                $('#info-cards-content > div:nth-child(3)').addClass('bounceInRight animated');
            }
        },
        beforeSlide: function (index) {
            console.log("beforeSlide", index);
        },
        endSlide: function (index) {
            console.log("endSlide", index);
        },
        mouseWheelEvents: true,
        mouseWheelDelay: false,
        mouseDragEvents: false,
        touchEvents: false,
        arrowKeyEvents: true,
        pagination: true,
        nthClasses: 2,
        detectHash: true
    });


});