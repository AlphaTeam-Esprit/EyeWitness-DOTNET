﻿using Microsoft.Owin;
using Owin;

[assembly: OwinStartup(typeof(EyeWitness.GUI.Identity.Startup))]
namespace EyeWitness.GUI.Identity
{
    public partial class Startup
    {
        public void Configuration(IAppBuilder app)
        {
            ConfigureAuth(app);
            app.MapSignalR();
        }
    }
}
