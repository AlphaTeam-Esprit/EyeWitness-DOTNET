﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.Entities
{
    public class SubscriptionsWC
    {
        [Key, Column(Order = 0)]
        public int WitnessCardId { get; set; }
        public virtual WitnessCard witnessCard { get; set; }
        [Key, Column(Order = 1)]
        public int UserId { get; set; }
        public virtual User user { get; set; }

        public DateTime subscriptionDate { get; set; }
        public string Type { get; set; }
    }
}
