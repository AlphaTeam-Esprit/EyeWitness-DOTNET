﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.Entities
{
   public  class Evaluation
    {
        public static string PICTURE_SEPARATOR = "|%%|";
        public DateTime evaluationDate { get; set; }
        public string comment { get; set; }
        public string video { get; set; }
        public int rating { get; set; }
        public string pictures { get; set; }

        [Key, Column(Order = 0)]
        public int WitnessCardId { get; set; }
        public virtual WitnessCard witnessCard { get; set; }
        [Key, Column(Order = 1)]
        public int UserId { get; set; }
        public virtual User user { get; set; }

        public string[] GetPicturesUrls()
        {
            return pictures.Split(new string[] { PICTURE_SEPARATOR }, StringSplitOptions.None);
        }
    }
}
