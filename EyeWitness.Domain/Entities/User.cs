﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.Entities
{
    public class User
    {
        public int Id { get; set; }
        public string firstName { get; set; }
        public string lastName { get; set; }

        public DateTime  birthDate { get; set; }

        public string password { get; set; }

        public string email { get; set; }

        public string address { get; set; }
        public string avatar { get; set; }

        public string phoneNumber { get; set; }

        public float noteXP { get; set; }

        public string role { get; set; }
        public string status { get; set; }

        public virtual ICollection<WitnessCard> witnessCardCreated { get; set; }

        public virtual ICollection<WitnessCard> witnessCardManaged { get; set; }

        public virtual ICollection<User> followers { get; set; }

        public virtual ICollection<Evaluation> evaluations { get; set; }

        public virtual ICollection<SubscriptionsWC> subscriptionsWC { get; set; }

        public virtual ICollection<SuggestionWC> SuggestingSuggestions { get; set; }
        public virtual ICollection<SuggestionWC> SuggestedSuggestions { get; set; }

    }
}
