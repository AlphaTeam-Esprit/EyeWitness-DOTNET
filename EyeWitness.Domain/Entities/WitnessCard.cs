﻿using EyeWitness.Domain.SearchCriterias;
using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.Entities
{
    public class WitnessCard
    {
        public int Id { get; set; }
        public string status { get; set; }
        public string name { get; set; }
        public string description { get; set; }
        public string picture { get; set; }
        public float note { get; set; }

        public virtual ICollection<SubscriptionsWC> subscriptionsWC { get; set; }
        public virtual ICollection<Evaluation> evaluations { get; set; }

        [ForeignKey("creator")]
        public int? creatorId { get; set; }
        public virtual User creator { get; set; }

        [ForeignKey("agent")]
        public int? agentId { get; set; }
        public virtual User agent { get; set; }

        public virtual ICollection<Competitions> competitions { get; set; }
        public virtual ICollection<Events> events { get; set; }

        [ForeignKey("category")]
        public virtual int CategoryId { get; set; }
        public virtual Category category { get; set; }

        public virtual ICollection<SuggestionWC> suggestionWC { get; set; }

        // Search Criterias
        public string Ambience { get; set; }
        public string OfferedServices { get; set; }
        public string MeansOfPayment { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }
        public string Address { get; set; }

        public int nbuser { get; set; }
    }
}
