﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.ComponentModel.DataAnnotations.Schema;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.Entities
{
    public class SuggestionWC
    {
        [Key, Column(Order = 0)]
        public int SuggestingUserId { get; set; }
        public virtual User SuggestingUser { get; set; }

        [Key, Column(Order = 1)]
        public int SuggestedUserId { get; set; }
        public virtual User SuggestedUser { get; set; }

        [Key, Column(Order = 2)]
        public int SuggestedCardId { get; set; }
        public virtual WitnessCard SuggestedCard { get; set; }

        public DateTime DateSuggestion { get; set; }
    }
}
