﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.Entities
{
    public class Criterias
    {
        public int Id { get; set; }
        public string name { get; set; }
        public virtual ICollection<ChoosenCriterias> choosenCriterias { get; set; }
    }
}
