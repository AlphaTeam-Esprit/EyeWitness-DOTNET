﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.Entities
{
    public class Category
    {
        public int Id { get; set; }
        public string name { get; set; }
        public String Status { get; set; }
        public virtual ICollection<WitnessCard> witnessCards { get; set; }
        public virtual ICollection<ChoosenCriterias> choosenCriterias { get; set; }
    }
}
