﻿namespace EyeWitness.Domain.SearchCriterias
{
    public enum AmbienceCriteria
    {
        ROMANTIC, FAMILY, PARTY, FRIENDS,
        BETWEEN_FRIENDS, ANNIVERSARY, EVENT
    }
}