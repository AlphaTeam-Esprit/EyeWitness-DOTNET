﻿namespace EyeWitness.Domain.SearchCriterias
{
    public enum MeansOfPaymentCriteria
    {
        CREDIT_CARD, CHECK, CASH, TICKET
    }
}