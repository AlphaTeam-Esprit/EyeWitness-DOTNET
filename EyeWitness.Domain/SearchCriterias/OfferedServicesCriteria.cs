﻿namespace EyeWitness.Domain.SearchCriterias
{
    public enum OfferedServicesCriteria
    {
        WIFI, PARKING, TV, ANIMALS_FRIENDLY, CARTES,
        DISABLED_PERSONS, KIDS_SPACE
    }
}