﻿namespace EyeWitness.Domain.SearchCriterias
{
    public enum OrderByCriteria
    {
        ALPHABETICAL, NUMBER_OF_SUBSCRIBERS, RATING
    }
}