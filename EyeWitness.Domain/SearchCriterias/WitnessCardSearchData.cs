﻿using EyeWitness.Domain.Entities;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace EyeWitness.Domain.SearchCriterias
{
    public class WitnessCardSearchData
    {
        public String Name { get; set; }
        public OrderByCriteria OrderBy { get; set; }
        public IEnumerable<int> CategoriesIds { get; set; }
        public IEnumerable<string> MeansOfPayment { get; set; }
        public IEnumerable<string> OfferedServices { get; set; }
        public IEnumerable<string> Ambiences { get; set; }
        public float Longitude { get; set; }
        public float Latitude { get; set; }

        public string Address { get; set; }
        public bool NearMe { get; set; }

        public bool IsNull()
        {
            return Name == null
                && OrderBy == OrderByCriteria.ALPHABETICAL
                && CategoriesIds == null
                && MeansOfPayment == null
                && OfferedServices == null
                && Ambiences == null
                && Longitude == 0
                && Latitude == 0
                && Address == null
                && NearMe == false;
        }
    }
}
